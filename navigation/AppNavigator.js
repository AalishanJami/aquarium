import React from 'react';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';

import MainTabNavigator from './MainTabNavigator';
import Login from '../screens/Login';
import AuthLoading from '../screens/AuthLoading';

export default createAppContainer(
  createSwitchNavigator({
    AuthLoading: AuthLoading,
    Main: MainTabNavigator,
    Login: Login,
  })
);
