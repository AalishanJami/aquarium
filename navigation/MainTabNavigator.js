import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator, createDrawerNavigator } from 'react-navigation';

import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import Schedule from '../screens/Schedule';

import VisitDetails from '../screens/VisitDetails';

import Customers from '../screens/Customers';
import ProjectHistory from '../screens/ProjectHistory';
import VisitLogStart from '../screens/VisitLogStart';
import VisitLogMid from '../screens/VisitLogMid';
import VisitLogEnd from '../screens/VisitLogEnd';
import AuthLoading from '../screens/AuthLoading';
import VisitNotifications from '../screens/VisitNotifications';


const config = Platform.select({
  web: { headerMode: null },
  default: {},
});

const HomeStack = createStackNavigator(
  {
    Home: HomeScreen,
    Schedule: Schedule,
    Customers: Customers,
    VisitDetails: VisitDetails,
    ProjectHistory: ProjectHistory,
    VisitNotifications: VisitNotifications,
    VisitLogStart: VisitLogStart,
    VisitLogMid : VisitLogMid,
    VisitLogEnd: VisitLogEnd,

  },
  config
);




const tabNavigator = createDrawerNavigator({
  "Home": {screen: HomeStack},
  "Schedule": {screen:Schedule}, 
  "Customers": {screen: Customers},
  "Notifications": {screen: VisitNotifications},
});

tabNavigator.path = '';

export default tabNavigator;
