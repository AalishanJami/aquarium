import React from "react";
import { Ionicons } from "@expo/vector-icons";
import axios from "axios";
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Dimensions,
  height,
  AsyncStorage,
  ProgressBarAndroid,
  ToastAndroid
} from "react-native";
import { Left, Right, Body, Icon, Picker } from "native-base";

class Customers extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      language: "",
      customerData: [],
      customerProjects: [],
      selectedCustomer: "",
      pickerLoaded: false,
      staff_id: "",
      customer_id: "",
      projects: {},
      isCustomerSelected: false,
      isLoading: false,
      isEmpty: false
    };
  }

  static navigationOptions = {
    header: null
  };

  async componentDidMount() {
    const { navigation } = this.props;
    var customer_id = navigation.getParam("customer_id", "NO-ID");
    this.state.staff_id = await AsyncStorage.getItem("staff_id");
    if(customer_id !== "NO-ID"){
      this.setState({selectedCustomer:customer_id});
      this.projects();
    }
    console.log(this.state.staff_id, "staff id");
    var self = this;
    console.log()
    axios
      .post(global.url + "/api/getCustomers", {
        staff_id: self.state.staff_id
      })
      .then(function(response) {
     
        if (response.data.success) {

          self.setState({customerData: response.data.data})

          self.setState({ pickerLoaded: true });
        } else {
          ToastAndroid.show("No Customers Found!", ToastAndroid.SHORT);
        }
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  async projects() {
    var self = this;
    self.setState({isEmpty: false})
    self.state.staff_id = await AsyncStorage.getItem("staff_id");
    console.log("staffid", self.state.staff_id);
    console.log("customerid", self.state.selectedCustomer);
    self.setState({ isCustomerSelected: false });
    self.setState({ isLoading: true });
    axios
      .post(global.url + "/api/getCustomerProjects", {
        staff_id: self.state.staff_id,
        customer_id: self.state.selectedCustomer
      })
      .then(function(response) {
        console.log(response.data);
        console.log(
          "staff" + self.state.staff_id + "cus" + self.state.selectedCustomer
        );
        self.setState({ isCustomerSelected: true });
        self.setState({ isLoading: false });
        if (response.data.success) {
          if(response.data.data.length > 0){
            self.setState({ customerProjects: response.data.data });
          }else {
            self.setState({isEmpty: true});
          }
          
          
        } else {
        }
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  render() {
    return (
      <View style={styles.container}>
        <View
          style={{
            backgroundColor: "#1095c4",
            width: "100%",
            height: 80,
            flexDirection: "row",
            paddingTop:24
          }}
        >
          <Left style={{ flex: 0.3 }}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Icon
                name="ios-arrow-back"
                style={{ color: "#fff", marginLeft: 14 }}
              />
            </TouchableOpacity>
          </Left>
          <Body style={{ flex: 1 }}>
            <Text
              style={{
                color: "#fff",
                fontSize: 17,
                letterSpacing: 1,
                fontWeight: "bold"
              }}
            >
              CUSTOMERS
            </Text>
          </Body>
          <Right style={{ flex: 0.3 }}>
            <TouchableOpacity
              onPress={() => this.props.navigation.openDrawer()}
              style={{ marginRight: 14 }}
            >
              <Icon name="md-menu" style={{ color: "#fff", marginLeft: 14 }} />
            </TouchableOpacity>
          </Right>
        </View>
        <ScrollView style={{ flex: 1 }}>
          <View
            style={{
              flex: 1,
              margin: 24
            }}
          >
            {this.state.pickerLoaded ? (
              <View style={{ borderColor: "#000", borderWidth: 0.7 }}>
                <Picker
                mode="dropdown"
                iosIcon={<Icon name="arrow-down" />}
                headerStyle={{ backgroundColor: "#1095c4" }}
                headerBackButtonTextStyle={{ color: "#fff" }}
                headerTitleStyle={{ color: "#fff" }}
                placeholder="Select a customer"
                  selectedValue={this.state.selectedCustomer}
                  style={{ height: 50, width: "100%" }}
                  onValueChange={value => {
                    this.setState({ selectedCustomer: value });
                    if (value !== "0") {
                      this.projects();
                      //   console.log(value);
                    }
                  }}
                >
                  <Picker.Item label="Select a Customer" value="0" />
                  {this.state.customerData.map(cs => (
                    <Picker.Item
                      label={cs.customer_name}
                      value={cs.id.toString()}
                      key={cs.id.toString()}
                    /> 
                  ))}
                </Picker>
              </View>
            ) : null}
            {this.state.isLoading ? (
              <ProgressBarAndroid style={{ color: "#1095c4", marginTop: 12 }} />
            ) : this.state.isCustomerSelected ? (
              this.state.isEmpty ? 
              <View
              style={{
                backgroundColor: "#1095c470",
                padding: 12,
                justifyContent: "center",
                alignItems: "center",
                marginTop: 12
              }}
            >
              <Text>No Projects to show!</Text>
            </View> : 
              this.state.customerProjects.map(cs => (
                
                <TouchableOpacity
                  key={cs.id.toString()}
                  onPress={() =>
                    this.props.navigation.navigate("ProjectHistory", {
                      itemId: 86,
                      project_id: cs.project.id.toString(),
                      customer_id: cs.customer.id.toString(),
                    })
                  }
                >
                  <View
                    style={{
                      backgroundColor: "#1095c4",
                      flexDirection: "row",
                      height: 120,
                      borderRadius: 4,
                      padding: 12,
                      marginTop: 12
                    }}
                  >
                    <View style={{ marginLeft: 4, flex: 1 }}>
                      <Text
                        style={{
                          fontSize: 16,
                          color: "#fff",
                          fontWeight: "bold"
                        }}
                      >
                        Project Name : {cs.project.project_name}
                      </Text>

                      <Text
                        style={{ fontSize: 16, color: "#fff", marginTop: 4 }}
                      >
                        Total number of visits: {cs.visit.length.toString()}
                      </Text>
                      <Text style={{ fontSize: 16, color: "#fff" }}>
                        Number of completed visits: {cs.plan_completed_visit}
                      </Text>
                      <Text style={{ fontSize: 16, color: "#fff" }}>
                        Remaining visits: {(cs.visit.length - parseInt(cs.plan_completed_visit)).toString()}
                      </Text>
                    </View>
                  </View>
                </TouchableOpacity>
          
             ))
            ) : null}
          </View>
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  }
});

export default Customers;
