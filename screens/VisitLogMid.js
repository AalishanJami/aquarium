import React from "react";
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TextInput,
  Picker,
  KeyboardAvoidingView
} from "react-native";
import { Left, Right, Body, Icon } from "native-base";
import { MaterialCommunityIcons, EvilIcons } from "@expo/vector-icons";

class VisitLogMid extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      project_id: "",
      customer_id: "",
      log_date: "",
      log_time: "",
      remarksArr: "",
      remarks1: "",
      remarks2: "",
      recommendation: "",
      fee: "",
      feeRemarks: "",
      visit_id: "",
      title_subtitle_remarks: "",
      log_remarks: "",
      log_recommended_action: "",
      log_user_comment: "",
      log_fees: "",
      log_fee_remarks: "",
      back_office:"",
      isCompleted: "",
      img: "",
      isSubmitted: ""
    };
  }
  static navigationOptions = {
    header: null
  };
  async componentDidMount() {
    const { navigation } = this.props;
    const visit_id = navigation.getParam("visit_id", "NO-ID");
    const project_id = navigation.getParam("project_id", "NO-ID");
    const customer_id = navigation.getParam("customer_id", "NO-ID");
    const log_date = navigation.getParam("log_date", "NO-ID");
    const log_time = navigation.getParam("log_time", "NO-ID");
    const remarksArr = navigation.getParam("remarksArr", "NO-ID");
    const remarks1 = navigation.getParam("remarks1", "NO-ID");
    var isCompleted = navigation.getParam("isCompleted", "NO-ID");
    this.setState({isCompleted: isCompleted});
    this.setState({ project_id });
    this.setState({ customer_id });
    this.setState({ log_date });
    this.setState({ log_time });
    this.setState({ remarksArr });
    this.setState({ remarks1 });
    this.setState({visit_id})
    var isSubmitted = navigation.getParam("isSubmitted", "NO-ID");
    this.setState({isSubmitted})

    console.log(
      project_id,
      customer_id,
      log_date,
      log_time,
      remarksArr,
      remarks1,
      visit_id
    );
    var img = navigation.getParam("img", "NO-ID");
    this.setState({img});
    console.log("img", img);

    var title_subtitle_remarks = navigation.getParam(
      "title_subtitle_remarks",
      "NO-ID"
    );
    var log_remarks = navigation.getParam("log_remarks", "NO-ID");
    var log_recommended_action = navigation.getParam(
      "log_recommended_action",
      "NO-ID"
    );
    var log_user_comment = navigation.getParam("log_user_comment", "NO-ID");
    var log_fees = navigation.getParam("log_fees", "NO-ID");
    var log_fee_remarks = navigation.getParam("log_fee_remarks", "NO-ID");
    this.setState({ visit_id });
    this.setState({ selectedProject: project_id });
    this.setState({ selectedCustomer: customer_id });
    this.setState({ title_subtitle_remarks });
    this.setState({ log_remarks });
    this.setState({ log_recommended_action });
    this.setState({ log_user_comment });
    this.setState({ log_fees });
    this.setState({ log_fee_remarks });
    console.log("log fee",log_fee_remarks);

    if(log_fees !== "NO-ID"){
      console.log("data is present");
     
      var remarks12arr = JSON.parse(log_remarks);
      console.log(remarks12arr);
      var remarks2 = remarks12arr[1];
      this.setState({remarks2: remarks2});
      this.setState({recommendation:log_recommended_action});
      this.setState({back_office:log_user_comment});
      this.setState({fee: log_fees});
      this.setState({feeRemarks: log_fee_remarks});
      console.log(log_fee_remarks)
    }
    else {
      console.log("firstvisit")
    }
  }
  render() {
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding">
        <View
          style={{
            backgroundColor: "#1095c4",
            width: "100%",
            height: 80,
            flexDirection: "row",
paddingTop:24
          }}
        >
          <Left style={{ flex: 0.5, flexDirection:"row" }}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={{flex:1}}>
              <Icon
                name="ios-arrow-back"
                style={{ color: "#fff", marginLeft: 14 }}
              />
            </TouchableOpacity>
          </Left>
          <Body style={{ flex: 1 }}>
            <Text
              style={{
                color: "#fff",
                fontSize: 17,
                letterSpacing: 1,
                fontWeight: "bold"
              }}
            >
              VISIT LOG
            </Text>
          </Body>
          <Right style={{ flex: 0.5 }}>
            <TouchableOpacity
              onPress={() => this.props.navigation.openDrawer()}
              style={{ marginRight: 14 }}
            >
              <Icon name="md-menu" style={{ color: "#fff", marginLeft: 14 }} />
            </TouchableOpacity>
          </Right>
        </View>
        <ScrollView style={{ flex: 1 }}>
          <View style={{ margin: 12 }}>
            <View style={{ marginLeft: 12, marginRight: 12 }}>
              <Text style={{ marginTop: 16 }}>Remarks 2</Text>
              <View style={{ marginTop: 8 }}>
                <TextInput
                  style={{
                    borderWidth: 1,
                    borderColor: "#000",
                    width: "100%",
                    height: 130,
                    paddingLeft: 12
                  }}
                  multiline={true}
                  onChangeText={text => this.setState({ remarks2: text })}
                  value={this.state.remarks2}
                ></TextInput>
              </View>

              <Text style={{ marginTop: 16 }}>Recommendation</Text>
              <View style={{ marginTop: 8 }}>
                <TextInput
                  style={{
                    borderWidth: 1,
                    borderColor: "#000",
                    width: "100%",
                    height: 130,
                    paddingLeft: 12
                  }}
                  multiline={true}
                  onChangeText={text => this.setState({ recommendation: text })}
                  value={this.state.recommendation}
                ></TextInput>
              </View>

              <Text style={{ marginTop: 16 }}>Back Office Comments</Text>
              <View
                style={{
                  marginTop: 8,
                  borderWidth: 1,
                  borderColor: "#000",
                  width: "100%",
                  height: 130,
                  paddingLeft: 12
                }}
              >
                <Text>{this.state.back_office}</Text>
              </View>

              <View style={{ flexDirection: "row", marginTop: 16 }}>
                <Text>Fees Amount</Text>
                <TextInput
                  style={{
                    borderColor: "#000",
                    borderWidth: 1,
                    width: 70,
                    marginLeft: 12,
                    paddingLeft: 12
                  }}
                  onChangeText={text => this.setState({ fee: text })}
                  value={this.state.fee}
                />
                <Text style={{ fontWeight: "bold", marginLeft: 12 }}>QAR</Text>
              </View>

              <Text style={{ marginTop: 16 }}>Fee Remarks</Text>
              <View style={{ marginTop: 8 }}>
                <TextInput
                  style={{
                    borderWidth: 1,
                    borderColor: "#000",
                    width: "100%",
                    height: 130,
                    paddingLeft: 12
                  }}
                  multiline={true}
                  onChangeText={text => this.setState({ feeRemarks: text })}
                  value={this.state.feeRemarks}
                ></TextInput>
              </View>
              <View style={{ flexDirection: "row" }}>
                <Left style={{ flex: 1 }}>
                  <TouchableOpacity
                    style={{ alignItems: "flex-end", marginTop: 16 }}
                    onPress={() => this.props.navigation.goBack()}
                  >
                    <View
                      style={{
                        backgroundColor: "#1095c4",
                        height: 40,
                        width: 90,
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                    >
                      <Text style={{ color: "#fff" }}>Back</Text>
                    </View>
                  </TouchableOpacity>
                </Left>
                <Body style={{ flex: 0.5 }} />
                <Right style={{ flex: 1 }}>
                  <TouchableOpacity
                    style={{ alignItems: "flex-end", marginTop: 16 }}
                    onPress={() =>
                      this.props.navigation.navigate("VisitLogEnd", {
                        itemId: 86,
                        project_id: this.state.project_id,
                        customer_id: this.state.customer_id,
                        log_date: this.state.log_date,
                        log_time: this.state.log_time,
                        remarksArr: this.state.remarksArr,
                        remarks1: this.state.remarks1,
                        remarks2: this.state.remarks2,
                        recommendation: this.state.recommendation,
                        fee: this.state.fee,
                        feeRemarks: this.state.feeRemarks,
                        visit_id: this.state.visit_id,
                        isCompleted:this.state.isCompleted,
                        img:this.state.img,
                        isSubmitted: this.state.isSubmitted
                      })
                    }
                  >
                    <View
                      style={{
                        backgroundColor: "#1095c4",
                        height: 40,
                        width: 90,
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                    >
                      <Text style={{ color: "#fff" }}>Next</Text>
                    </View>
                  </TouchableOpacity>
                </Right>
              </View>
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //paddingTop: 24,
    backgroundColor: "#fff"
  }
});

export default VisitLogMid;
