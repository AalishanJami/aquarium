import React, { useState, useEffect } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { Camera } from 'expo-camera';

export default class CameraScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hasPermission: false,
      recording: false,
      video: "",
      type : Camera.Constants.Type.back
    };
    this.getPermissions();
  }

  getPermissions = async () => {
    const { status } = await Camera.requestPermissionsAsync();
    console.log(status);
  };

  handlePhoto = async () => {
    // if(this.cameraRef){
      let photo = await this.camera.takePictureAsync();
      console.log(photo);
    // } 
  }

  render(){
    return (
      <View style={{ flex: 1 }}>
        <Camera style={{ flex: 1 }} type={this.state.type} ref={ref => { this.camera = ref}}>
          <View
            style={{
              flex: 1,
              backgroundColor: 'transparent',
              flexDirection: 'row',
            }}>
            {/* <TouchableOpacity
              style={{
                flex: 0.1,
                alignSelf: 'flex-end',
                alignItems: 'center',
              }}
              onPress={() => 
                  this.state.type === Camera.Constants.Type.back
                    ? Camera.Constants.Type.front
                    : Camera.Constants.Type.back
              }>
              <Text style={{ fontSize: 18, marginBottom: 10, color: 'white' }}> Flip</Text>
            </TouchableOpacity> */}
            <TouchableOpacity
              style={{
               
                alignSelf: 'flex-end',
                alignItems: 'center',
                justifyContent:"center",
                marginBottom: 50,
                width:"100%"
              }}
              onPress={this.handlePhoto} >
                <View style={{height: 100, width: 100, borderRadius: 50, borderWidth: 2, borderColor: "#fff", backgroundColor: "#000"}}></View>
              {/* <Text style={{ fontSize: 18, marginBottom: 10, color: 'white' }}> Take Picture</Text> */}
            </TouchableOpacity>
          </View>
        </Camera>
      </View>
    );
  }

  
}