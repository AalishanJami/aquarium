import { View } from "native-base";
import React from 'react';
import { Ionicons } from '@expo/vector-icons';
import { Entypo } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';
import { EvilIcons } from '@expo/vector-icons';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity
} from 'react-native';
import { Left, Right, Body, Icon, width } from "native-base";
import { bold } from "ansi-colors";


class VisitDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      customername: "Customer name",
      projectname: "Project name",
      vs: [],
      isLoading: true,
      remarksArr:[]
    }

  }
  static navigationOptions = {
    header: null
  };
  componentDidMount() {
    const { navigation } = this.props;
    var customer_name = navigation.getParam("customer_name", "NO-ID");
    var project_name = navigation.getParam("project_name", "NO-ID");
    var vs = navigation.getParam("vs", "NO-ID");
    console.log(vs);
    this.setState({ customer_name });
    this.setState({ vs });
    this.setState({ project_name })
    var remarks= JSON.parse(vs.title_subtitle_remarks);
    this.setState({remarksArr: remarks})
    
    this.setState({ isLoading: false })
  }
  render() {
    return (
      <View style={styles.container}>

        <View style={{
          backgroundColor: "#1095c4",
          width: "100%",
          height: 80,
          flexDirection: "row",
          paddingTop: 24
        }}>

          <Left style={{ flex: 0.3 }}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Icon
                name="ios-arrow-back"
                style={{ color: "#fff", marginLeft: 14 }}
              />
            </TouchableOpacity>

          </Left>
          <Body style={{ flex: 1 }}>
            <Text style={{ color: "#fff", fontSize: 17, letterSpacing: 1, fontWeight: "bold" }}>VISIT DETAILS</Text>
          </Body>
          <Right style={{ flex: 0.3 }}><TouchableOpacity
            onPress={() => this.props.navigation.openDrawer()}
            style={{ marginRight: 14 }}
          >
            <Icon name="md-menu" style={{ color: "#fff", marginLeft: 14 }} />
          </TouchableOpacity></Right>
        </View>

        <ScrollView style={{ flex: 1 }}>
          {this.state.isLoading ? null :
            <View style={{ backgroundColor: "#1095c4", margin: 16, paddingBottom: 20, borderRadius: 5 }}>
              <Text style={{ color: "white", fontSize: 14, fontWeight: "bold", paddingLeft: 10, paddingTop: 10 }}>{this.state.customer_name}</Text>
              <Text style={{ color: "white", fontSize: 14, fontWeight: "bold", paddingLeft: 10, paddingTop: 5 }}>{this.state.project_name}</Text>

              <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                {this.state.vs.log_visit_status === 'Completed' ?
                  <TouchableOpacity style={{ width: width, height: 40 }} >
                    <View style={{ backgroundColor: "#1095c4", borderRadius: 20, marginLeft: 15, marginTop: 15 }}>
                      <Ionicons name="md-checkmark" style={{ color: "#fff", marginLeft: 14, fontSize: 20 }} />
                      <Text style={{ color: "white", fontSize: 11, marginTop: 5 }}>Completed</Text>
                    </View>
                  </TouchableOpacity> : null}
                {this.state.vs.log_visit_status === 'In Progress' ?
                  <TouchableOpacity style={{ width: width, height: 40 }} >
                    <View style={{ backgroundColor: "#1095c4", borderRadius: 20, marginLeft: 10, marginTop: 15 }}>
                      <Entypo name="dots-three-horizontal" style={{ color: "#fff", marginLeft: 14, fontSize: 20 }} />
                      <Text style={{ color: "white", fontSize: 11, marginTop: 5 }}>In Progress</Text>
                    </View>
                  </TouchableOpacity> : null}
                {this.state.vs.log_visit_status === 'Approved' ?
                  <TouchableOpacity style={{ width: width, height: 40 }} >
                    <View style={{ backgroundColor: "#1095c4", borderRadius: 20, marginLeft: 10, marginTop: 15 }}>
                      <Entypo name="dots-three-horizontal" style={{ color: "#fff", marginLeft: 14, fontSize: 20 }} />
                      <Text style={{ color: "white", fontSize: 11, marginTop: 5 }}>In Progress</Text>
                    </View>
                  </TouchableOpacity> : null}
                {this.state.vs.log_visit_status === 'Pending' ?
                  <TouchableOpacity style={{ width: width, height: 40 }} >
                    <View style={{ backgroundColor: "#1095c4", borderRadius: 20, marginLeft: 10, marginTop: 15 }}>
                      <AntDesign name="clockcircleo" style={{ color: "#fff", marginLeft: 14, fontSize: 20 }} />
                      <Text style={{ color: "white", fontSize: 11, marginTop: 5 }}>Pending</Text>
                    </View>
                  </TouchableOpacity> : null}
                {this.state.vs.log_visit_status === 'Cancelled' ?
                  <TouchableOpacity style={{ width: width, height: 40 }} >
                    <View style={{ backgroundColor: "#1095c4", borderRadius: 20, marginLeft: 10, marginTop: 15 }}>
                      <Entypo name="cross" style={{ color: "#fff", marginLeft: 14, fontSize: 20 }} />
                      <Text style={{ color: "white", fontSize: 11, marginTop: 5 }}>Cancelled</Text>
                    </View>
                  </TouchableOpacity> : null}
                {this.state.vs.log_visit_status === 'Rescheduled' ?
                  <TouchableOpacity style={{ width: width, height: 40 }} >
                    <View style={{ backgroundColor: "#1095c4", borderRadius: 20, marginLeft: 10, marginTop: 15 }}>
                      <Entypo name="calendar" style={{ color: "#fff", marginLeft: 14, fontSize: 20 }} />
                      <Text style={{ color: "white", fontSize: 11, marginTop: 5 }}>Rescheduled</Text>
                    </View>
                  </TouchableOpacity> : null}
              </View>

              <View style={{
                flexDirection: 'row',
                alignItems: 'flex-start', paddingTop: 25
              }}>
                <Text style={{ color: "white", fontSize: 14, fontWeight: "bold", paddingLeft: 10, paddingTop: 5 }}>Visit Date:</Text>
                <Text style={{ color: "white", fontSize: 14, paddingTop: 6 }}>{this.state.vs.log_date}</Text>
              </View>
              <View style={{
                flexDirection: 'row',
                alignItems: 'flex-start',
              }}>
                <Text style={{ color: "white", fontSize: 14, fontWeight: "bold", paddingLeft: 10, paddingTop: 5 }}>Visit Time:</Text>
                <Text style={{ color: "white", fontSize: 14, paddingTop: 6 }}>{this.state.vs.log_time}</Text>
              </View>
              <View style={{
                flexDirection: 'row',
                alignItems: 'flex-start'
              }}>
                <Text style={{ color: "white", fontSize: 14, fontWeight: "bold", paddingLeft: 10, paddingTop: 5 }}>Staff Name:</Text>
                <Text style={{ color: "white", fontSize: 14, paddingTop: 6 }}>{this.state.vs.employee}</Text>
              </View>
              <View style={{
                flexDirection: 'row',
                alignItems: 'flex-start'
              }}>
                <Text style={{ color: "white", fontSize: 14, fontWeight: "bold", paddingLeft: 10, paddingTop: 5 }}>Remarks 1:</Text>
                <Text style={{ color: "white", fontSize: 14, paddingTop: 6 }}>{JSON.parse(this.state.vs.log_remarks)[0]}</Text>
              </View>
              <View style={{
                flexDirection: 'row',
                alignItems: 'flex-start'
              }}>
                <Text style={{ color: "white", fontSize: 14, fontWeight: "bold", paddingLeft: 10, paddingTop: 5 }}>Remarks 2:</Text>
                <Text style={{ color: "white", fontSize: 14, paddingTop: 6 }}>{JSON.parse(this.state.vs.log_remarks)[1]}</Text>
              </View>

              <View style={{
                flexDirection: 'row',
                alignItems: 'flex-start'
              }}>
                <Text style={{ color: "white", fontSize: 14, fontWeight: "bold", paddingLeft: 10, paddingTop: 5 }}>Back Office Comments:</Text>
                <Text style={{ color: "white", fontSize: 14, paddingTop: 6 }}>{this.state.vs.log_user_comment}</Text>
              </View>

              <View style={{
                flexDirection: 'row',
                alignSelf: 'flex-start', paddingLeft: 10, paddingTop: 10
              }}>
                <Text style={{ color: "white", fontSize: 14, fontWeight: "bold" }}>Fees amount Collected:  </Text>
                <Text style={{ color: "white", fontSize: 14, fontWeight: "bold" }}>{this.state.vs.log_fees} </Text>
                <Text style={{ color: "white", fontSize: 14, fontWeight: "bold" }}>QAR</Text>
              </View>

              <View style={{
                flexDirection: 'row',
                alignSelf: 'flex-start', paddingLeft: 10, paddingTop: 10
              }}>
                <Text>
                  <Text style={{ color: "white", fontSize: 14, paddingTop: 10, fontWeight: "bold" }}>Fees Remarks:  </Text>
                  <Text style={{ color: "white", fontSize: 14, paddingTop: 6 }}>{this.state.vs.log_fee_remarks}</Text>
                </Text>

              </View>

              <View style={{ marginTop: 12,marginLeft: 12, marginRight:12 }}>
                <View style={{ flexDirection: "row", height: 40 }}>
                  <View
                    style={{
                      flex: 1,
                      borderWidth: 1,
                      borderColor: "#fff",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Text style={{color:"#fff"}}>Task</Text>
                  </View>
                  <View
                    style={{
                      flex: 1,
                      borderWidth: 1,
                      borderColor: "#fff",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Text style={{color:"#fff"}}>Sub-task</Text>
                  </View>
                  <View
                    style={{
                      flex: 1,
                      borderWidth: 1,
                      borderColor: "#fff",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Text style={{color:"#fff"}}>Remarks</Text>
                  </View>
              
                </View>

 {this.state.remarksArr.map((r, i) => (
                      <View style={{ flexDirection: "row", height: 40 }}>
                        <View
                          style={{
                            flex: 1,
                            borderWidth: 1,
                            borderColor: "#fff",
                            justifyContent: "center",
                            alignItems: "center"
                          }}
                        >
                          <Text style={{color:"#fff"}}>{r.title.name}</Text>
                        </View>
                        <View
                          style={{
                            flex: 1,
                            borderWidth: 1,
                            borderColor: "#fff",
                            justifyContent: "center",
                            alignItems: "center"
                          }}
                        >
                          <Text style={{color:"#fff"}}>{r.subtitle.name}</Text>
                        </View>
                        <View
                          style={{
                            flex: 1,
                            borderWidth: 1,
                            borderColor: "#fff",
                            justifyContent: "center",
                            alignItems: "center"
                          }}
                        >
                          <Text style={{color:"#fff"}}>{r.remarks}</Text>
                        </View>
                        
                      </View>
                    ))
                  }
              </View>

              {this.state.vs.visit_log_attachments && this.state.vs.visit_log_attachments.length> 0 ?
                <ScrollView horizontal={true} style={{margin: 12}}>
                  {this.state.vs.visit_log_attachments.map(img=>(
                    <View style={{flexDirection:"row"}}>
                      {img.log_attachment_name.split('.')[1]==='mp4' ? null : 
                    <Image
                      source={{ uri: global.url + '/images/' + img.log_attachment_name }}
                      style={{ width: 100, height: 100, marginLeft: 12 }}
                  /> }
                </View> ))}
                </ScrollView>
                 : null}

            </View>
          }
        </ScrollView>

      </View>
    );
  }

}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',

  },
  row: {
    flex: 1,
    flexDirection: "row"
  }
});

export default VisitDetails;