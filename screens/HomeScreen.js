import * as WebBrowser from "expo-web-browser";
import React from "react";
import { Ionicons } from '@expo/vector-icons';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Dimensions,
  AsyncStorage
} from "react-native";
import { FontAwesome, AntDesign } from "@expo/vector-icons";
import { MonoText } from "../components/StyledText";
import { Left, Right, Body, Icon } from "native-base";
var height = Dimensions.get("window").height;

class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    console.log(height / 4);
  }

  static navigationOptions = {
    header: null
  };

  render() {
    return (
      <View style={styles.container}>
     

        <View
          style={{
            backgroundColor: "#1095c4",
            width: "100%",
            height: 80,
            flexDirection: "row",
            paddingTop:24
          }}
        >
          <Left style={{ flex: 0.3 }}>
            <TouchableOpacity
              onPress={() => this.props.navigation.openDrawer()}
  style={{marginRight: 14}}
            >
              <Icon name="md-menu" style={{ color: "#fff", marginLeft: 14 }} />
            </TouchableOpacity>
          </Left>
          <Body style={{ flex: 1 }}>
            <Text
              style={{
                color: "#fff",
                fontSize: 17,
                letterSpacing: 1,
                fontWeight: "bold"
              }}
            >
              AQUATICS WORLD
            </Text>
          </Body>
          <Right style={{ flex: 0.3 }}>
            <TouchableOpacity
            
              onPress={() => {AsyncStorage.removeItem("staff_id");this.props.navigation.navigate("Login")}}
            >
              <Icon
                name="ios-log-out"
                style={{ color: "#fff", marginRight: 14 }}
              />
            </TouchableOpacity>
          </Right>
        </View>
        <View
          style={{
            justifyContent: "center",
            alignItems: "center",
            flex: 1
          }}
        >
          <View style={{ alignItems: "center", justifyContent: "center" }}>
            <View
              style={{
                flex: 1,
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <TouchableOpacity onPress={()=>this.props.navigation.navigate('Schedule')}>
              <View
                style={{
                  backgroundColor: "#1095c4",
                  height: 120,
                  width: 120,
                  borderRadius: 10,
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <AntDesign name="calendar" size={90} color="#fff" />
              </View>

              <Text
                style={{
                  fontWeight: "bold",
                  color: "#1095c4",
                  marginTop: 12,
                  fontSize: 16
                }}
              >
                MY SCHEDULE
              </Text>
              </TouchableOpacity>
            </View>

            <View style={{ flex: 1, alignItems: "center" }}>
            <TouchableOpacity style={{alignItems: "center"}} onPress={()=> this.props.navigation.navigate('Customers')}>
              <View
                style={{
                  backgroundColor: "#1095c4",
                  height: 120,
                  width: 120,
                  borderRadius: 10,
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <FontAwesome name="users" size={70} color="#fff" />
              </View>

              <Text
                style={{
                  fontWeight: "bold",
                  color: "#1095c4",
                  marginTop: 12,
                  fontSize: 16
                }}
              >
                CUSTOMERS
              </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>

      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    //paddingTop: 24,
    flex: 0.8,
    backgroundColor: '#fff',
    justifyContent:"center",
    alignItems:"center"
    
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  Ionicons: {
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },

});

export default HomeScreen;
