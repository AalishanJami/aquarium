import React from "react";

import { View, Text, Image, Dimensions, TouchableOpacity, KeyboardAvoidingView, ProgressBarAndroid } from "react-native";
import { Container, Header, Content, Item, Input, Icon, Toast, Spinner } from "native-base";
import { EvilIcons } from '@expo/vector-icons';
import {AsyncStorage} from 'react-native';
import axios from "axios";
var width = Dimensions.get('window').width;
class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      staff_id: "",
      password: "",
      passwordEmpty: false,
      staffEmpty: false,
      staffData:{},
      showProgress: false,
      
    };
  }

  componentDidMount() {
    // const default_year= new Date().getFullYear();
    // const default_month= new Date().getMonth() + 1;
    // const default_day= new Date().getDate();
    // this.setState({dateStr: default_year + "-" + default_month + "-" + default_day});
    // console.log(default_year + "-" + default_month + "-" + default_day);
    AsyncStorage.removeItem("staff_id");
  }


  authenticate= ()=>{
    var self = this;
    // console.log("staffid", self.state.staff_id);
    // console.log("password", self.state.password);
    self.setState({showProgress: true})
    axios.post(global.url + '/api/login',{
      "staff_id" : self.state.staff_id,
      "password" : self.state.password
    })
    .then(function(response){
     console.log(response.data);
 
      if(response.data.success){
        
        self.state.staffData = response.data.data;
        console.log(self.state.staffData.id);
        self.props.navigation.navigate('Main');
        Toast.show({
          text: response.data.message,
          buttonText: "Okay",
          duration: 3000
        }) 

        // ToastAndroid.show(response.data.message, ToastAndroid.LONG);
        AsyncStorage.setItem('staff_id', self.state.staffData.id.toString());
        AsyncStorage.setItem('staff_password', self.state.password);
        AsyncStorage.setItem('staff_obj', JSON.stringify(response.data));
        self.setState({showProgress: false})
      //   AsyncStorage.setItem(ACCESS_TOKEN, JSON.stringify(responseData), (err)=> {
      //     if(err){
      //         console.log("an error");
      //         throw err;
      //     }
      //     console.log("success");
      // }).catch((err)=> {
      //     console.log("error is: " + err);
      // });
      }
      else{
        Toast.show({
          text: response.data.message,
          buttonText: "Okay",
          duration: 3000
        })
      }
      
        self.setState({showProgress: false})
      
    })
    .catch(function(error){
      console.log(error);
      self.setState({showProgress: false})
    })
  }

  

  render() {
    return (
      <KeyboardAvoidingView style={{flex: 1}} behavior="padding" enabled>
      <View style={{ justifyContent: "center", alignItems: "center", flex: 1 }}>
        <Image
          style={{ width: 120, height: 120, marginBottom: 50 }}
          source={require("../assets/images/logoicon.png")}
        />
        <Item style={{ marginLeft: 24, marginRight: 24 }}>
          <EvilIcons
            active name="envelope" size={22} color="#808080" />
          <Input placeholder="Staff ID" placeholderTextColor="#808080" style={{ fontSize: 16 }} value={this.state.staff_id} onChangeText={(text)=>{this.setState({staff_id: text})}}/>
        </Item>
        {this.state.staffEmpty ? <Text>Enter Staff ID</Text> : null}
        <Item style={{ marginLeft: 24, marginRight: 24 }}>
          <EvilIcons
            active name="lock" size={22} color="#808080" />
          <Input placeholder="Password" secureTextEntry={true} placeholderTextColor="#808080" style={{ fontSize: 16 }} value={this.state.password} onChangeText={(text)=>{this.setState({password: text})}}/>
        </Item>
        {this.state.staffEmpty ? <Text>Enter Password</Text> : null}
        {this.state.showProgress ? <Spinner color="#1095c4" />: 
        <TouchableOpacity onPress={this.authenticate} style={{ width: width, height: 40, marginTop: 30 }}>
          <View style={{ backgroundColor: "#1095c4", borderRadius: 20, marginLeft: 24, marginRight: 24, height: 40, alignItems: "center", justifyContent: "center" }}>
            <Text style={{ color: "#fff", fontWeight: "bold", fontSize: 18 }}>LOG IN</Text>
          </View>
        </TouchableOpacity> }
        {/* <TouchableOpacity style={{ width: width, height: 40, marginTop: 30 }} onPress={() => this.props.navigation.navigate('Main')}>
          <View style={{ backgroundColor: "#1095c4", borderRadius: 20, marginLeft: 24, marginRight: 24, height: 40, alignItems: "center", justifyContent: "center" }}>
            <Text style={{ color: "#fff", fontWeight: "bold", fontSize: 18 }}>CANCEL</Text>
          </View>
        </TouchableOpacity> */}
      </View>
      </KeyboardAvoidingView>
    );
  }
}

export default Login;
