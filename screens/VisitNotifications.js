import * as WebBrowser from "expo-web-browser";
import React from "react";
import { Ionicons } from "@expo/vector-icons";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  AsyncStorage,
  ProgressBarAndroid,
  TouchableOpacity
} from "react-native";
import { Feather, AntDesign } from "@expo/vector-icons";
import { Left, Right, Body, Icon } from "native-base";
import axios from "axios";

class VisitNotifications extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      visits: [],
      staff_id: "",
      isEmpty: false
    };
  }

  async componentDidMount() {
    this.setState({ isLoading: true });
    this.state.staff_id = await AsyncStorage.getItem("staff_id");
    this.getNotifications();
  }

  getNotifications = () => {
    var self = this;
    self.setState({isLoading: true})
    axios
      .post(global.url + "/api/getRemarkedVisits", {
        staff_id: self.state.staff_id
      })
      .then(function(response) {
        if(response.data.success){
          self.setState({ isLoading: false });
          self.setState({isEmpty: false})
          console.log(response.data);
          self.setState({ visits: response.data.data });
        }
        else{
          self.setState({isLoading: false})
          self.setState({isEmpty: true})
        }
        

      })
      .catch(function(error) {
        console.log(error);
        self.setState({isLoading: false})

      });
  };

  static navigationOptions = {
    header: null
  };

  render() {
    return (
      <View style={{flex:1}}>
        <View
          style={{
            backgroundColor: "#1095c4",
            width: "100%",
            height: 80,
            flexDirection: "row",
paddingTop:24
          }}
        >
          <Left style={{ flex: 0.5 }}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Icon
                name="ios-arrow-back"
                style={{ color: "#fff", marginLeft: 14 }}
              />
            </TouchableOpacity>
          </Left>
          <Body style={{ flex: 1 }}>
            <Text
              style={{
                color: "#fff",
                fontSize: 17,
                letterSpacing: 1,
                fontWeight: "bold"
              }}
            >
              MY NOTIFICATIONS
            </Text>
          </Body>
          <Right style={{ flex: 0.5 }}>
            <TouchableOpacity
              onPress={this.getNotifications}
              style={{ marginRight: 14 }}
            >
              <Feather
                name="refresh-ccw"
                style={{ color: "#fff", marginLeft: 14 }}
              />
            </TouchableOpacity>
          </Right>
        </View>

        <View
          style={{
            flex: 1,
            marginLeft: 12,
            marginRight: 12
          }}
        >
          {this.state.isLoading ? (
            <ProgressBarAndroid style={{ color: "#1095c4", marginTop: 12 }} />
          ) :( this.state.isEmpty? <View
            style={{
              backgroundColor: "#1095c470",
              padding: 12,
              justifyContent: "center",
              alignItems: "center",
              marginTop: 12
            }}
          >
            <Text>No Notifications to show!</Text>
          </View>: this.state.visits.length > 0 ? this.state.visits.map(cs => (
            <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate("VisitLogStart", {
                itemId: 86,
                project_id: cs.project.id.toString(),
                customer_id: cs.customer.id.toString(),
                date: cs.log_date,
                time: cs.log_time,
                visit_id: cs.id.toString(),
                title_subtitle_remarks: cs.title_subtitle_remarks,
                log_remarks: cs.log_remarks,
                log_recommended_action: cs.log_recommended_action,
                log_user_comment: cs.log_user_comment,
                log_fees: cs.log_fees,
                log_fee_remarks: cs.log_fee_remarks,
                isCompleted: cs.log_visit_status === "Approved" ? "true" : "false",
                img: JSON.stringify(cs.visit_log_attachments)
              })
            }
          >
                <View
                  style={{
                    backgroundColor: "#1095c4",
                    flexDirection: "row",
                    height: 120,
                    borderRadius: 4,
                    padding: 12,
                    marginTop: 12
                  }}
                >
                  
                  <View style={{ marginLeft: 4, flex: 1 }}>
                    <Text
                      style={{
                        fontSize: 16,
                        color: "#fff",
                        fontWeight: "bold"
                      }}
                    >
                      Project Name : {cs.project.project_name}
                    </Text>

                    <Text style={{ fontSize: 16, color: "#fff", marginTop: 4 }}>
                      Customer Name: {cs.customer.customer_name}
                    </Text>

                    <Text style={{ fontSize: 16, color: "#fff" }}>
                      Visit Details:{" "}
                      {cs.log_visit_status === "Approved"
                        ? "Visit is accepted, please complete the visit"
                        : "Visit is rejected, please complete back office remarks"}
                    </Text>
                  </View>
                  
                </View>
                </TouchableOpacity>
              )):null)}
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    //paddingTop: 24,
    backgroundColor: "#fff"
  }
});

export default VisitNotifications;
