import React from "react";
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TextInput,
  Picker,
  ToastAndroid,
  ProgressBarAndroid,
  Modal,
  KeyboardAvoidingView
} from "react-native";
import { Left, Right, Body, Icon, Toast, Spinner } from "native-base";
import {
  MaterialCommunityIcons,
  EvilIcons,
  Ionicons
} from "@expo/vector-icons";
import * as ImagePicker from "expo-image-picker";
import Constants from "expo-constants";
import * as Permissions from "expo-permissions";
import axios from "axios";
import { NavigationActions, StackActions } from "react-navigation";
import { Camera } from 'expo-camera';
import { Audio } from 'expo-av';

class VisitLogEnd extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      project_id: "",
      customer_id: "",
      log_date: "",
      log_time: "",
      remarksArr: "",
      remarks1: "",
      remarks2: "",
      recommendation: "",
      fee: "",
      feeRemarks: "",
      image: null,
      isCompleted: "",
      isImageNew: false,
      isImageOld: false,
      img: "",
      imageArray: [],
      isLoading: false,
      oldImageArray: [],
      cameraShow: false,
      isSubmitted: "",
      cameraShowView: false,
      videoShow : false
    };
  }
  static navigationOptions = {
    header: null
  };
  componentDidMount = () => {
    const { navigation } = this.props;
    const project_id = navigation.getParam("project_id", "NO-ID");
    const customer_id = navigation.getParam("customer_id", "NO-ID");
    const log_date = navigation.getParam("log_date", "NO-ID");
    const log_time = navigation.getParam("log_time", "NO-ID");
    const remarksArr = navigation.getParam("remarksArr", "NO-ID");
    const remarks1 = navigation.getParam("remarks1", "NO-ID");
    const visit_id = navigation.getParam("visit_id", "NO-ID");
    var isCompleted = navigation.getParam("isCompleted", "NO-ID");
    var isSubmitted = navigation.getParam("isSubmitted", "NO-ID");
    this.setState({ isSubmitted });
    this.setState({ isCompleted: isCompleted });
    console.log(isCompleted);
    const remarks2 = navigation.getParam("remarks2", "NO-ID");
    const recommendation = navigation.getParam("recommendation", "NO-ID");
    const fee = navigation.getParam("fee", "NO-ID");
    const feeRemarks = navigation.getParam("feeRemarks", "NO-ID");
    var img = navigation.getParam("img", "NO-ID");

    if (img !== "NO-ID") {
      this.setState({ oldImageArray: JSON.parse(img) });
      this.setState({ isImageOld: true });
      console.log("img");
      console.log(img);
      console.log(this.state.oldImageArray);
    }
    this.setState({ project_id });
    this.setState({ customer_id });
    this.setState({ log_date });
    this.setState({ log_time });
    this.setState({ remarksArr });
    this.setState({ remarks1 });
    this.setState({ visit_id });
    this.setState({ remarks2 });
    this.setState({ recommendation });
    this.setState({ fee });
    this.setState({ feeRemarks });

    console.log(
      project_id,
      customer_id,
      log_date,
      log_time,
      remarksArr,
      remarks1,
      remarks2,
      recommendation,
      fee,
      feeRemarks,
      visit_id
    );
    this.getPermissions();
  };

  getPermissions = async () => {
    const { status } = await Camera.requestPermissionsAsync();
    console.log(status);
  };
  handlePhoto = async () => {
      this.setModalCameraView(!this.state.cameraShowView);
      let photo = await this.camera.takePictureAsync();
      var uri = photo.uri;
      var image = {
        type: "image",
        uri: uri
      };
      
      this.state.imageArray.push(image);
      this.setState({ imageArray: this.state.imageArray });
      console.log(this.state.imageArray);
      this.setState({ isImageOld: false });

      console.log(photo);
  }
  _StopRecord = async () => {
    this.setState({ recording: false }, async () => {
       const status = this.camera.stopRecording();
      //console.log (status);
    });
  };

  _StartRecord = async () => {
    if (this.camera) {
      this.setState({ recording: true }, async () => {
        await Audio.requestPermissionsAsync()
        const video = await this.camera.recordAsync();
        await this.setState({ video });
        console.log(video);
        this.setModalVideo(!this.state.videoShow)
          var uri = video.uri;
          var image = {
            type: "video",
            uri: uri
          }
          this.state.imageArray.push(image);
          this.setState({ imageArray: this.state.imageArray });
          console.log(this.state.imageArray);
          this.setState({isImageOld: false});

      });
    }
  };
  addVisit = () => {
    var self = this;
    self.setState({ isLoading: true });
    var remarks = [];
    remarks.push(self.state.remarks1);
    remarks.push(self.state.remarks2);
    remarks_string = JSON.stringify(remarks);
    console.log(remarks_string);
    axios
      .post(global.url + "/api/updateVisitLog", {
        visit_id: self.state.visit_id,
        project_id: self.state.project_id,
        customer_id: self.state.customer_id,
        log_date: self.state.log_date,
        log_time: self.state.log_time,
        title_subtitle_remarks: self.state.remarksArr,
        log_remarks: remarks_string,
        log_recommended_action: self.state.recommendation,
        log_fees: self.state.fee,
        log_fee_remarks: self.state.feeRemarks
      })
      .then(function(response) {
        console.log(response.data);
        if (self.state.isImageOld) {
          Toast.show({
            text: "Submitted",
            buttonText: "Okay",
            duration: 3000
          });
          // ToastAndroid.show('Submitted', ToastAndroid.SHORT);

          const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: "Schedule" })]
          });
          self.props.navigation.dispatch(resetAction);

          self.setState({ isLoading: false });
        } else {
          if (self.state.imageArray.length > 0) {
            self.uploadImage();
          } else {
            Toast.show({
              text: "Submitted",
              buttonText: "Okay",
              duration: 3000
            });
            //ToastAndroid.show('Submitted', ToastAndroid.SHORT);
            const resetAction = StackActions.reset({
              index: 0,
              actions: [NavigationActions.navigate({ routeName: "Schedule" })]
            });
            self.props.navigation.dispatch(resetAction);
            self.setState({ isLoading: false });
          }
        }
      })
      .catch(function(error) {
        console.log(error);
        Toast.show({
          text: "Something Went Wrong!",
          buttonText: "Okay",
          duration: 3000
        });
        ToastAndroid.show("Something went wrong!", ToastAndroid.SHORT);
      });
  };
  completeVisit = () => {
    var self = this;
    self.setState({ isLoading: true });
    var remarks = [];
    remarks.push(self.state.remarks1);
    remarks.push(self.state.remarks2);
    remarks_string = JSON.stringify(remarks);
    console.log(remarks_string);
    axios
      .post(global.url + "/api/completeVisitLog", {
        visit_id: self.state.visit_id,
        project_id: self.state.project_id,
        customer_id: self.state.customer_id,
        log_date: self.state.log_date,
        log_time: self.state.log_time,
        title_subtitle_remarks: self.state.remarksArr,
        log_remarks: remarks_string,
        log_recommended_action: self.state.recommendation,
        log_fees: self.state.fee,
        log_fee_remarks: self.state.feeRemarks
      })
      .then(function(response) {
        if (self.state.isImageOld) {
          Toast.show({
            text: "Completed",
            buttonText: "Okay",
            duration: 3000
          });
          //ToastAndroid.show('Completed', ToastAndroid.SHORT);
          self.props.navigation.navigate("Home");
          self.setState({ isLoading: false });
        } else {
          self.uploadImage();
        }
      })
      .catch(function(error) {
        console.log(error);
        Toast.show({
          text: "Something went wrong!",
          buttonText: "Okay",
          duration: 3000
        });
        //ToastAndroid.show('Something went wrong!', ToastAndroid.SHORT);
      });
  };

  uploadImage = () => {
    let formData = new FormData();
    var self = this;
    self.setState({ isLoading: true });
    var i;
    if (self.state.imageArray.length > 0) {
      for (i = 0; i < self.state.imageArray.length; i++) {
        var ext = self.state.imageArray[i].uri.substring(
          self.state.imageArray[i].uri.lastIndexOf(".") + 1
        );
        const newFile = {
          uri: self.state.imageArray[i].uri,
          type: self.state.imageArray[i].type + "/" + ext,
          name: "img." + ext
        };
        console.log(newFile);
        formData.append("fileName[]", newFile);
      }
      console.log(formData);
    } else {
      console.log("no new image added");
    }

    formData.append("visit_id", self.state.visit_id);

    axios
      .post(global.url + "/api/visitImage", formData)
      .then(function(response) {
        console.log(response.data);
        Toast.show({
          text: "Submitted",
          buttonText: "Okay",
          duration: 3000
        });
        //ToastAndroid.show('Submitted', ToastAndroid.SHORT);
        const resetAction = StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: "Schedule" })]
        });
        self.props.navigation.dispatch(resetAction);
        self.setState({ isLoading: false });
      })
      .catch(function(error) {
        console.log(error);
        Toast.show({
          text: "Submitted Went Wrong!",
          buttonText: "Okay",
          duration: 3000
        });
        //ToastAndroid.show('Something went wrong!', ToastAndroid.SHORT);
        self.setState({ isLoading: false });
      });
  };
  setModalCamera(visible) {
    this.setState({ cameraShow: visible });
  }
  setModalVideo(visible) {
    this.setState({ videoShow: visible });
  }
  setModalCameraView(visible) {
    this.setState({ cameraShowView: visible });
  }

  render() {
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding">
        <View
          style={{
            backgroundColor: "#1095c4",
            width: "100%",
            height: 80,
            flexDirection: "row",
            paddingTop: 24
          }}
        >
          <Left style={{ flex: 0.5, flexDirection: "row" }}>
            <TouchableOpacity
              onPress={() => this.props.navigation.goBack()}
              style={{ flex: 1 }}
            >
              <Icon
                name="ios-arrow-back"
                style={{ color: "#fff", marginLeft: 14 }}
              />
            </TouchableOpacity>
          </Left>
          <Body style={{ flex: 1 }}>
            <Text
              style={{
                color: "#fff",
                fontSize: 17,
                letterSpacing: 1,
                fontWeight: "bold"
              }}
            >
              VISIT LOG
            </Text>
          </Body>
          <Right style={{ flex: 0.5 }}>
            <TouchableOpacity
              onPress={() => this.props.navigation.openDrawer()}
              style={{ marginRight: 14 }}
            >
              <Icon name="md-menu" style={{ color: "#fff", marginLeft: 14 }} />
            </TouchableOpacity>
          </Right>
        </View>
        <ScrollView style={{ flex: 1 }}>
          <View style={{ margin: 12 }}>
            <Modal
              animationType="slide"
              transparent={true}
              visible={this.state.cameraShow}
              onRequestClose={() => {
                console.log("modal closed");
                this.setModalCamera(!this.state.cameraShow);
              }}
            >
              <View
                style={{
                  justifyContent: "center",
                  alignItems: "center",
                  backgroundColor: "#00000080",
                  height: "100%"
                }}
              >
                <TouchableOpacity
                  onPress={() => {
                    this.setModalCamera(!this.state.cameraShow);
                  }}
                >
                  <Icon name="md-close" />
                </TouchableOpacity>
                <View
                  style={{
                    height: 100,
                    width: "80%",
                    backgroundColor: "#fff",
                    alignItems: "center"
                  }}
                >
                  <TouchableOpacity
                    style={{
                      padding: 12,
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                    onPress={() => {
                      this.setModalCamera(!this.state.cameraShow);
                      this.setModalCameraView(!this.state.cameraShowView);
                      //this.props.navigation.navigate("Camera");
                    }}
                  >
                    <Text style={{ fontSize: 18 }}>Take Picture</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{
                      padding: 12,
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                    onPress={() => {
                      this.setModalCamera(!this.state.cameraShow);
                      this.setModalVideo(!this.state.videoShow);
                    }}
                  >
                    <Text style={{ fontSize: 18 }}>Record Video</Text>
                  </TouchableOpacity>
                  {/* <TouchableOpacity style={{padding: 12, justifyContent:"center", alignItems:"center"}} onPress={this._pickImage}>
                <Text style={{fontSize: 18}}>Select from Gallery</Text>
              </TouchableOpacity> */}
                </View>
              </View>
            </Modal>
            
            <Modal
              animationType="slide"
              transparent={true}
              visible={this.state.cameraShowView}
              onRequestClose={() => {
                console.log("modal closed");
                this.setModalCameraView(!this.state.cameraShowView);
              }}
            >
               <View style={{ flex: 1 }}>
                <Camera style={{ flex: 1 }} type={this.state.type} ref={ref => { this.camera = ref}}>
                  <View
                    style={{
                      flex: 1,
                      backgroundColor: 'transparent',
                      flexDirection: 'row',
                    }}>
                    {/* <TouchableOpacity
                      style={{
                        flex: 0.1,
                        alignSelf: 'flex-end',
                        alignItems: 'center',
                      }}
                      onPress={() => 
                          this.state.type === Camera.Constants.Type.back
                            ? Camera.Constants.Type.front
                            : Camera.Constants.Type.back
                      }>
                      <Text style={{ fontSize: 18, marginBottom: 10, color: 'white' }}> Flip</Text>
                    </TouchableOpacity> */}
                    <TouchableOpacity
                      style={{
                      
                        alignSelf: 'flex-end',
                        alignItems: 'center',
                        justifyContent:"center",
                        marginBottom: 50,
                        width:"100%"
                      }}
                      onPress={this.handlePhoto} >
                        <View style={{height: 100, width: 100, borderRadius: 50, borderWidth: 2, borderColor: "#fff", backgroundColor: "#000"}}></View>
                      {/* <Text style={{ fontSize: 18, marginBottom: 10, color: 'white' }}> Take Picture</Text> */}
                    </TouchableOpacity>
                  </View>
                </Camera>
              </View>
            </Modal>
            
            <Modal
              animationType="slide"
              transparent={true}
              visible={this.state.videoShow}
              onRequestClose={() => {
                console.log("modal closed");
                this.setModalVideo(!this.state.videoShow);
              }}
            >
                <View style={{ flex: 1 }}>
                <Camera style={{ flex: 1 }} type={this.state.type} ref={ref => { this.camera = ref}}>
                  <View
                    style={{
                      flex: 1,
                      backgroundColor: "transparent",
                      flexDirection: "row"
                    }}
                  >
                    {/* <TouchableOpacity
                      style={{
                        flex: 0.1,
                        alignSelf: "flex-end",
                        alignItems: "center"
                      }}
                      onPress={() => {
                          this.state.type === Camera.Constants.Type.back
                            ? Camera.Constants.Type.front
                            : Camera.Constants.Type.back
                      }}
                    >
                      <Text style={{ fontSize: 18, marginBottom: 10, color: "white" }}>
                        {" "}
                        Flip
                      </Text>
                    </TouchableOpacity> */}
        
                    {/* <TouchableOpacity
                      style={{
                        flex: 0.1,
                        alignSelf: "flex-end",
                        alignItems: "center"
                      }}
                      onPress={this._StartRecord}
                    >
                      <Text style={{ fontSize: 18, marginBottom: 10, color: "white" }}>
                        {" "}
                        Take Video
                      </Text>
                    </TouchableOpacity> */}
                    {this.state.recording ? 
                    <TouchableOpacity
                    style={{
                    
                      alignSelf: 'flex-end',
                      alignItems: 'center',
                      justifyContent:"center",
                      marginBottom: 50,
                      width:"100%"
                    }}
                    onPress={this._StopRecord} >
                      <View style={{height: 100, width: 100, borderRadius: 50, borderWidth: 2, borderColor: "#fff", backgroundColor: "red"}}></View>
                    {/* <Text style={{ fontSize: 18, marginBottom: 10, color: 'white' }}> Take Picture</Text> */}
                  </TouchableOpacity>
                    : 
                    <TouchableOpacity
                    style={{
                      
                      alignSelf: 'flex-end',
                      alignItems: 'center',
                      justifyContent:"center",
                      marginBottom: 50,
                      width:"100%"
                    }}
                    onPress={this._StartRecord} >
                      <View style={{height: 100, width: 100, borderRadius: 50, borderWidth: 2, borderColor: "#fff", backgroundColor: "#000"}}></View>
                    {/* <Text style={{ fontSize: 18, marginBottom: 10, color: 'white' }}> Take Picture</Text> */}
                    </TouchableOpacity> }
                    {/* <TouchableOpacity
                      style={{
                        flex: 0.1,
                        alignSelf: "flex-end",
                        alignItems: "center"
                      }}
                      onPress={this._saveVideo}
                    >
                      <Text style={{ fontSize: 18, marginBottom: 10, color: "white" }}>
                        {" "}
                        Save Video
                      </Text>
                    </TouchableOpacity> */}
                  </View>
                </Camera>
              </View>
            </Modal>
            

            <View style={{ marginLeft: 12, marginRight: 12 }}>
              <Text style={{ marginTop: 16 }}>Photo or Video</Text>

              <View style={{ marginTop: 8, flexDirection: "row" }}>
                <TouchableOpacity
                  onPress={() => {
                    this.setModalCamera(!this.state.cameraShow);
                  }}
                >
                  <View
                    style={{
                      backgroundColor: "#1095c4",
                      width: 100,
                      height: 100,
                      justifyContent: "center",
                      alignItems: "center",
                      borderRadius: 10
                    }}
                  >
                    <Icon
                      name="md-image"
                      style={{ color: "#fff", fontSize: 70 }}
                    />
                  </View>
                </TouchableOpacity>
              </View>
              {this.state.imageArray.length > 0 ? (
                <ScrollView horizontal={true} style={{ marginTop: 12 }}>
                  {this.state.imageArray.map((img, i) => (
                    <View style={{ flexDirection: "row" }}>
                      <Image
                        source={{ uri: img.uri }}
                        style={{ width: 100, height: 100, marginLeft: 12 }}
                      />
                      <TouchableOpacity
                        onPress={() => {
                          this.state.imageArray.splice(i, 1);
                          this.setState({ imageArray: this.state.imageArray });
                          console.log(i);
                        }}
                      >
                        <Ionicons
                          name="md-close"
                          style={{ marginLeft: 6, fontSize: 14 }}
                        />
                      </TouchableOpacity>
                    </View>
                  ))}
                </ScrollView>
              ) : null}

              {this.state.isImageOld &&
              this.state.oldImageArray &&
              this.state.oldImageArray.length > 0 ? (
                <ScrollView horizontal={true} style={{ marginTop: 12 }}>
                  {this.state.oldImageArray.map(img => (
                    <View style={{ flexDirection: "row" }}>
                      <Image
                        source={{
                          uri: global.url + "/images/" + img.log_attachment_name
                        }}
                        style={{ width: 100, height: 100, marginLeft: 12 }}
                      />
                    </View>
                  ))}
                </ScrollView>
              ) : null}

              <View style={{ flexDirection: "row" }}>
                <Left style={{ flex: 1 }}>
                  <TouchableOpacity
                    style={{ alignItems: "flex-end", marginTop: 16 }}
                    onPress={() => this.props.navigation.goBack()}
                  >
                    <View
                      style={{
                        backgroundColor: "#1095c4",
                        height: 40,
                        width: 90,
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                    >
                      <Text style={{ color: "#fff" }}>Back</Text>
                    </View>
                  </TouchableOpacity>
                </Left>
                <Body style={{ flex: 0.5 }} />

                <Right style={{ flex: 1 }}>
                  {this.state.isCompleted === "true" ? null : this.state
                      .isSubmitted === "true" ? null : (
                    <TouchableOpacity
                      style={{ alignItems: "flex-end", marginTop: 16 }}
                      onPress={this.addVisit}
                    >
                      <View
                        style={{
                          backgroundColor: "#1095c4",
                          height: 40,
                          width: 90,
                          justifyContent: "center",
                          alignItems: "center"
                        }}
                      >
                        <Text style={{ color: "#fff" }}>Save</Text>
                      </View>
                    </TouchableOpacity>
                  )}
                </Right>
              </View>
              {this.state.isCompleted === "true" ? (
                <TouchableOpacity
                  style={{ alignItems: "flex-end", marginTop: 16 }}
                  onPress={this.completeVisit}
                >
                  <View
                    style={{
                      backgroundColor: "#1095c4",
                      height: 40,
                      width: 90,
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Text style={{ color: "#fff" }}>Submit</Text>
                  </View>
                </TouchableOpacity>
              ) : null}
              {this.state.isLoading ? <Spinner color="#1095c4" /> : null}
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //paddingTop: 24,
    backgroundColor: "#fff"
  }
});

export default VisitLogEnd;
