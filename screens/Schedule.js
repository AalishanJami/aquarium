import React from "react";
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TextInput,
  DatePickerAndroid,
  AsyncStorage,
  ProgressBarAndroid,
  ToastAndroid,
  Modal
} from "react-native";
import { Left, Right, Body, Icon } from "native-base";
import { MaterialCommunityIcons, EvilIcons } from "@expo/vector-icons";
import axios from "axios";
import { Calendar, CalendarList, Agenda } from "react-native-calendars";

class Schedule extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      date: new Date()
        .toISOString("de-DE", { timeZone: "Asia/Riyadh" })
        .substr(0, 10),
      isLoading: true,
      staff_id: "",
      dateStr: "",
      data: [],
      isEmpty: true,
      checkedDates: {},
      isLoadingCalendar: false,
      isCalendarOpen: false,
      arrivedConfirm: false,
      rescheduleConfirm: false,
      visitidformodal: ""
    };
  }
  static navigationOptions = {
    header: null
  };
  componentDidMount() {
    // const default_year= new Date().getFullYear();
    // const default_month= new Date().getMonth() + 1;
    // const default_day= new Date().getDate();
    // this.setState({dateStr: default_year + "-" + default_month + "-" + default_day});
    // console.log(default_year + "-" + default_month + "-" + default_day);
    var date = new Date()
      .toISOString("de-DE", { timeZone: "Asia/Riyadh" })
      .substr(0, 10);
    console.log(date, "date at start");
    this.setState({ dateStr: date });
    this.getAllDates();
  }

  insertZero = n => {
    return n < 10 ? "0" + n : n;
  };

  setModalVisibleArrived(visible) {
    this.setState({ arrivedConfirm: visible });
  }

  setModalVisibleReSchedule(visible) {
    this.setState({ rescheduleConfirm: visible });
  }

  showAndroidDatePicker = async () => {
    try {
      const { action, year, month, day } = await DatePickerAndroid.open({
        date: this.state.date
      });
      if (action !== DatePickerAndroid.dismissedAction) {
        var date = new Date(year, month, day);
        this.setState({ date: date });
        console.log(date);
        var new_month = month + 1;
        var zeroed_month = new_month < 10 ? "0" + new_month : new_month;
        var zeroed_date = day < 10 ? "0" + day : day;
        this.setState(
          { dateStr: year + "-" + zeroed_month + "-" + zeroed_date },
          this.getDateVisits
        );
      }
    } catch ({ code, message }) {
      console.warn("Cannot open date picker", message);
    }
  };
  async getDateVisits() {
    var self = this;
    self.setState({ isCalendarOpen: false });
    self.state.staff_id = await AsyncStorage.getItem("staff_id");
    console.log(self.state.dateStr);
    self.setState({ isLoading: true });
    self.setState({ isEmpty: true });
    console.log("date ts");
    console.log(self.state.dateStr);
    axios
      .post(global.url + "/api/getDateVisits", {
        staff_id: self.state.staff_id,
        date: self.state.dateStr
      })
      .then(function(response) {
        if (response.data.success) {
          self.setState({ data: response.data.data });
          self.setState({ isLoading: false });
          self.setState({ isEmpty: false });
        } else {
          self.setState({ isLoading: false });
        }
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  async getAllDates() {
    var self = this;
    self.setState({ isLoading: true });
    self.state.staff_id = await AsyncStorage.getItem("staff_id");
    axios
      .post(global.url + "/api/getAllVisitDates", {
        staff_id: self.state.staff_id
      })
      .then(function(response) {
        console.log(response.data);
        if (response.data.success) {
          var i;
          var date;
          var obj = {};
          for (i = 0; i < response.data.data.length; i++) {
            date = response.data.data[i].log_date;
            if (response.data.data[i].log_visit_status === "Pending") {
              obj[date] = {
                selected: true,
                marked: true,
                selectedColor: "green"
              };
            } else {
              obj[date] = {
                selected: true,
                marked: true,
                selectedColor: "red"
              };
            }
          }
          console.log(obj);
          self.setState({ checkedDates: obj });
          self.setState({ isLoadingCalendar: true });

          self.getDateVisits();
        } else {
          self.setState({ isLoading: false });
          self.setState({ isEmpty: true });
          self.setState({ isLoadingCalendar: true });
        }
      })
      .catch(function(error) {
        console.log(error);
        self.getDateVisits();
      });
  }
  render() {
    return (
      <View style={styles.container}>
        <View
          style={{
            backgroundColor: "#1095c4",
            width: "100%",
            height: 80,
            flexDirection: "row",
            paddingTop: 24
          }}
        >
          <Left style={{ flex: 0.5 }}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("Home")}
            >
              <Icon
                name="ios-arrow-back"
                style={{ color: "#fff", marginLeft: 14 }}
              />
            </TouchableOpacity>
          </Left>
          <Body style={{ flex: 1 }}>
            <Text
              style={{
                color: "#fff",
                fontSize: 17,
                letterSpacing: 1,
                fontWeight: "bold"
              }}
            >
              MY SCHEDULE
            </Text>
          </Body>
          <Right style={{ flex: 0.5 }}>
            <TouchableOpacity
              onPress={() => this.props.navigation.openDrawer()}
              style={{ marginRight: 14 }}
            >
              <Icon name="md-menu" style={{ color: "#fff", marginLeft: 14 }} />
            </TouchableOpacity>
          </Right>
        </View>

        <View
          style={{ marginTop: 12, flexDirection: "row", alignItems: "center" }}
        >
          <TouchableOpacity
            onPress={() => {
              this.setState({ isCalendarOpen: true });
            }}
            style={{ width: 170 }}
          >
            <View
              style={{
                borderBottomWidth: 0.5,
                borderBottomColor: "#000",
                marginLeft: 24,
                padding: 12
              }}
            >
              <Text>{this.state.dateStr}</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              this.setState({ isCalendarOpen: true });
            }}
          >
            <EvilIcons
              style={styles.searchIcon}
              style={{ color: "#1095c4" }}
              name="calendar"
              size={28}
              color="grey"
            />
          </TouchableOpacity>
        </View>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.rescheduleConfirm}
          onRequestClose={() => {
            console.log("modal closed");
            this.setModalVisibleReSchedule(!this.state.rescheduleConfirm);
          }}
        >
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "#00000080",
              height: "100%"
            }}
          >
            <View
              style={{
                height: 130,
                width: "80%",
                backgroundColor: "#fff",
                alignItems: "center"
              }}
            >
              <Text style={{ fontSize: 18, margin: 12 }}>
                Are you sure you want to be rescheduled?
              </Text>
              <View style={{ flexDirection: "row", width: "100%" }}>
                <TouchableOpacity
                  style={{ flex: 1, margin: 12 }}
                  onPress={() => {
                    this.setModalVisibleReSchedule(
                      !this.state.rescheduleConfirm
                    );
                    var self = this;
                    self.setState({ isLoading: true });
                    axios
                      .post(global.url + "/api/markAsReScheduled", {
                        visit_id: this.state.visitidformodal
                      })
                      .then(function(response) {
                        if (response.data.success) {
                          ToastAndroid.show(
                            "Raised for Rescheduling!",
                            ToastAndroid.SHORT
                          );

                          self.getAllDates();
                        } else {
                          ToastAndroid.show(
                            "An Error Occured",
                            ToastAndroid.SHORT
                          );
                          self.setState({ isLoading: false });
                        }
                      })
                      .catch(function(error) {
                        console.log(error);
                        ToastAndroid.show(
                          "An Error Occured",
                          ToastAndroid.SHORT
                        );
                        self.setState({ isLoading: false });
                      });
                  }}
                >
                  <View
                    style={{
                      backgroundColor: "#03BB85",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Text style={{ color: "#fff", fontSize: 18 }}>Yes</Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{ flex: 1, margin: 12 }}
                  onPress={() => {
                    this.setModalVisibleReSchedule(
                      !this.state.rescheduleConfirm
                    );
                  }}
                >
                  <View
                    style={{
                      backgroundColor: "#03BB85",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Text style={{ color: "#fff", fontSize: 18 }}>No</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>

        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.arrivedConfirm}
          onRequestClose={() => {
            console.log("modal closed");
            this.setModalVisibleArrived(!this.state.arrivedConfirm);
          }}
        >
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "#00000080",
              height: "100%"
            }}
          >
            <View
              style={{
                height: 130,
                width: "80%",
                backgroundColor: "#fff",
                alignItems: "center"
              }}
            >
              <Text style={{ fontSize: 18, margin: 12 }}>
                Are you sure you have arrived?
              </Text>
              <View style={{ flexDirection: "row", width: "100%" }}>
                <TouchableOpacity
                  style={{ flex: 1, margin: 12 }}
                  onPress={() => {
                    this.setModalVisibleArrived(!this.state.arrivedConfirm);
                    var self = this;
                    self.setState({ isLoading: true });
                    axios
                      .post(global.url + "/api/markAsArrived", {
                        visit_id: this.state.visitidformodal
                      })
                      .then(function(response) {
                        if (response.data.success) {
                          ToastAndroid.show(
                            "Marked as Arrived!",
                            ToastAndroid.SHORT
                          );
                          self.getAllDates();
                        } else {
                          ToastAndroid.show(
                            "An Error Occured",
                            ToastAndroid.SHORT
                          );
                          self.setState({ isLoading: false });
                        }
                      })
                      .catch(function(error) {
                        console.log(error);
                        ToastAndroid.show(
                          "An Error Occured",
                          ToastAndroid.SHORT
                        );
                        self.setState({ isLoading: false });
                      });
                  }}
                >
                  <View
                    style={{
                      backgroundColor: "#03BB85",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Text style={{ color: "#fff", fontSize: 18 }}>Yes</Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{ flex: 1, margin: 12 }}
                  onPress={() => {
                    this.setModalVisibleArrived(!this.state.arrivedConfirm);
                  }}
                >
                  <View
                    style={{
                      backgroundColor: "#03BB85",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Text style={{ color: "#fff", fontSize: 18 }}>No</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>

        {this.state.isCalendarOpen ? (
          this.state.isLoadingCalendar ? (
            <Calendar
              // Initially visible month. Default = Date()
              current={new Date(this.state.date)}
              // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
              // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
              // maxDate={this.state.androidDateend ? this.state.androidDateend : "2022-01-01"}
              // Handler which gets executed on day press. Default = undefined
              onDayPress={day => {
                console.log(day);
                var date = new Date(day.dateString);
                console.log(date);
                console.log("date obj");
                this.setState({ date: date });
                var new_month = day.month > 9 ? day.month : "0" + day.month;
                var new_day = day.day > 9 ? day.day : "0" + day.day;
                console.log(day.dateString);
                this.setState({ dateStr: day.dateString }, this.getDateVisits);
              }}
              // Handler which gets executed on day long press. Default = undefined
              onDayLongPress={day => {
                console.log("selected day", day);
              }}
              // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
              monthFormat={"MMM yyyy"}
              // Handler which gets executed when visible month changes in calendar. Default = undefined
              onMonthChange={month => {
                console.log("month changed", month);
              }}
              // Hide month navigation arrows. Default = false
              hideArrows={false}
              // Replace default arrows with custom ones (direction can be 'left' or 'right')
              // renderArrow={(direction) => (<Arrow />)}
              // Do not show days of other months in month page. Default = false
              hideExtraDays={true}
              // If hideArrows=false and hideExtraDays=false do not switch month when tapping on greyed out
              // day from another month that is visible in calendar page. Default = false
              disableMonthChange={false}
              // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday.
              firstDay={1}
              // Hide day names. Default = false
              hideDayNames={false}
              // Show week numbers to the left. Default = false
              showWeekNumbers={false}
              // // Handler which gets executed when press arrow icon left. It receive a callback can go back month
              // onPressArrowLeft={substractMonth => substractMonth()}
              // // Handler which gets executed when press arrow icon left. It receive a callback can go next month
              // onPressArrowRight={addMonth => addMonth()}

              markedDates={this.state.checkedDates}
            />
          ) : null
        ) : null}

        <View style={{ margin: 12 }}>
          {this.state.isLoading ? (
            <ProgressBarAndroid style={{ color: "#1095c4", marginTop: 12 }} />
          ) : this.state.isEmpty ? (
            <View
              style={{
                backgroundColor: "#1095c470",
                padding: 12,
                justifyContent: "center",
                alignItems: "center",
                marginTop: 12
              }}
            >
              <Text>No Visits to show!</Text>
            </View>
          ) : (
            this.state.data.map(d => (
              <View>
                <View
                  style={{
                    backgroundColor: "#1095c4",
                    flexDirection: "row",
                    height: 80,
                    borderRadius: 4,
                    padding: 12,
                    marginTop: 12
                  }}
                >
                  <View
                    style={{
                      width: 80,
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Text
                      style={{
                        color: "#fff",
                        fontSize: 20,
                        fontWeight: "bold"
                      }}
                    >
                      {new Date(d.log_date)
                        .toLocaleString("default", { month: "long" })
                        .substr(0, 3)}
                    </Text>
                    <Text
                      style={{
                        color: "#fff",
                        fontSize: 32,
                        fontWeight: "bold",
                        marginTop: -5
                      }}
                    >
                      {new Date(d.log_date).getDate()}
                    </Text>
                  </View>
                  <View
                    style={{
                      backgroundColor: "#fff",
                      height: "100%",
                      width: 1
                    }}
                  />
                  <View style={{ marginLeft: 4 }}>
                    <Text
                      style={{
                        fontSize: 14,
                        color: "#fff",
                        fontWeight: "bold"
                      }}
                    >
                      {d.log_time}
                    </Text>
                    <TouchableOpacity
                      onPress={() =>
                        this.props.navigation.navigate("Customers", {
                          itemId: 86,
                          customer_id: d.customer.id.toString()
                        })
                      }
                    >
                      <Text
                        style={{
                          fontSize: 14,
                          color: "#fff",
                          fontWeight: "bold"
                        }}
                      >
                        {d.customer.customer_name.substring(0, 16) + "..."}
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={() =>
                        this.props.navigation.navigate("ProjectHistory", {
                          itemId: 86,
                          project_id: d.project.id.toString(),
                          customer_id: d.customer.id.toString()
                        })
                      }
                    >
                      <Text
                        style={{
                          fontSize: 14,
                          color: "#fff",
                          fontWeight: "bold"
                        }}
                      >
                        {d.project.project_name.substring(0, 16) + "..."}
                      </Text>
                    </TouchableOpacity>
                  </View>

                  <Left>
                    <View
                      style={{ alignSelf: "flex-end", flexDirection: "row" }}
                    >
                      <View>
                        <Text
                          style={{
                            color: "#fff",
                            fontSize: 15,
                            fontWeight: "bold"
                          }}
                        >
                          {d.last_visit === null ? null : "Last Visit Date"}
                        </Text>
                        <View
                          style={{
                            backgroundColor: "#fff",
                            width: 75,
                            padding: 5,
                            marginTop: 6,
                            borderRadius: 2,
                            marginLeft: 16,
                            alignItems: "center",
                            justifyContent: "center"
                          }}
                        >
                          <Text style={{ color: "#1095c4", fontSize: 10 }}>
                            {d.last_visit === null
                              ? "First Visit"
                              : d.last_visit}
                          </Text>
                        </View>
                      </View>
                      {d.log_visit_status === "Scheduled" ||
                      d.log_visit_status === "Arrived" ? (
                        <TouchableOpacity
                          onPress={() =>
                            this.props.navigation.navigate("VisitLogStart", {
                              itemId: 86,
                              project_id: d.project.id.toString(),
                              customer_id: d.customer.id.toString(),
                              date: d.log_date,
                              time: d.log_time,
                              visit_id: d.id.toString()
                            })
                          }
                        >
                          <Icon
                            name="md-more"
                            style={{ marginLeft: 12, color: "#fff" }}
                          />
                        </TouchableOpacity>
                      ) : (
                        <TouchableOpacity
                          onPress={() =>
                            this.props.navigation.navigate("VisitLogStart", {
                              itemId: 86,
                              project_id: d.project.id.toString(),
                              customer_id: d.customer.id.toString(),
                              date: d.log_date,
                              time: d.log_time,
                              visit_id: d.id.toString(),
                              title_subtitle_remarks: d.title_subtitle_remarks,
                              log_remarks: d.log_remarks,
                              log_recommended_action: d.log_recommended_action,
                              log_user_comment: d.log_user_comment,
                              log_fees: d.log_fees,
                              log_fee_remarks: d.log_fee_remarks,
                              isCompleted:
                                d.log_visit_status === "Approved"
                                  ? "true"
                                  : "false",
                              isSubmitted:
                                d.log_visit_status === "Pending"
                                  ? "true"
                                  : "false",
                              img: JSON.stringify(d.visit_log_attachments)
                            })
                          }
                        >
                          <Icon
                            name="md-more"
                            style={{ marginLeft: 12, color: "#fff" }}
                          />
                        </TouchableOpacity>
                      )}
                    </View>
                  </Left>
                </View>
                <View
                  style={{ height: 40, flexDirection: "row", marginTop: 4 }}
                >
                  {d.log_visit_status === "Arrived" ||
                  d.log_visit_status === "Pending" ||
                  d.log_visit_status === "Approved" ||
                  d.log_visit_status === "In Progress" ? (
                    <TouchableOpacity style={{ flex: 1 }} disabled={true}>
                      <View
                        style={{
                          justifyContent: "center",
                          alignItems: "center",
                          borderTopLeftRadius: 5,
                          borderBottomLeftRadius: 5,
                          height: 40,
                          backgroundColor: "#f8f8f8"
                        }}
                      >
                        <Text>In Progress</Text>
                      </View>
                    </TouchableOpacity>
                  ) : (
                    <TouchableOpacity
                      style={{ flex: 1 }}
                      onPress={() => {
                        this.setModalVisibleArrived(!this.state.arrivedConfirm);
                        this.setState({ visitidformodal: d.id.toString() });
                      }}
                    >
                      <View
                        style={{
                          justifyContent: "center",
                          alignItems: "center",
                          borderTopLeftRadius: 5,
                          borderBottomLeftRadius: 5,
                          height: 40,
                          backgroundColor: "#03BB85"
                        }}
                      >
                        <Text>Arrived</Text>
                      </View>
                    </TouchableOpacity>
                  )}

                  <TouchableOpacity
                    style={{ flex: 1 }}
                    onPress={() => {
                      this.setModalVisibleReSchedule(
                        !this.state.rescheduleConfirm
                      );
                      this.setState({ visitidformodal: d.id.toString() });
                    }}
                  >
                    <View
                      style={{
                        justifyContent: "center",
                        alignItems: "center",
                        height: 40,
                        borderBottomRightRadius: 5,
                        borderTopRightRadius: 5,
                        backgroundColor: "#ff5349"
                      }}
                    >
                      <Text style={{ color: "#fff" }}>Re Schedule</Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
            ))
          )}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //paddingTop: 24,
    backgroundColor: "#fff"
  }
});

export default Schedule;
