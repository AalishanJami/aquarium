import React, { useState, useEffect } from "react";
import { Text, View, TouchableOpacity } from "react-native";
import { Camera } from "expo-camera";
import { MediaLibrary } from "expo-media-library";
import { Audio } from 'expo-av';

export default class VideoScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hasPermission: false,
      recording: false,
      video: null,
      type : Camera.Constants.Type.back,
      uri: ""
    };
    this.getPermissions();
    
  }
  getPermissions = async () => {
    const { status } = await Camera.requestPermissionsAsync();
 
  };

  _StopRecord = async () => {
    this.setState({ recording: false }, async () => {
       const status = this.camera.stopRecording();
      //console.log (status);
    });
  };

  _StartRecord = async () => {
    if (this.camera) {
      this.setState({ recording: true }, async () => {
        await Audio.requestPermissionsAsync()
        const video = await this.camera.recordAsync();
        await this.setState({ video });
        console.log(video);
        
      });
    }
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <Camera style={{ flex: 1 }} type={this.state.type} ref={ref => { this.camera = ref}}>
          <View
            style={{
              flex: 1,
              backgroundColor: "transparent",
              flexDirection: "row"
            }}
          >
            {/* <TouchableOpacity
              style={{
                flex: 0.1,
                alignSelf: "flex-end",
                alignItems: "center"
              }}
              onPress={() => {
                  this.state.type === Camera.Constants.Type.back
                    ? Camera.Constants.Type.front
                    : Camera.Constants.Type.back
              }}
            >
              <Text style={{ fontSize: 18, marginBottom: 10, color: "white" }}>
                {" "}
                Flip
              </Text>
            </TouchableOpacity> */}
 
            {/* <TouchableOpacity
              style={{
                flex: 0.1,
                alignSelf: "flex-end",
                alignItems: "center"
              }}
              onPress={this._StartRecord}
            >
              <Text style={{ fontSize: 18, marginBottom: 10, color: "white" }}>
                {" "}
                Take Video
              </Text>
            </TouchableOpacity> */}
            {this.state.recording ? 
            <TouchableOpacity
            style={{
             
              alignSelf: 'flex-end',
              alignItems: 'center',
              justifyContent:"center",
              marginBottom: 50,
              width:"100%"
            }}
            onPress={this._StopRecord} >
              <View style={{height: 100, width: 100, borderRadius: 50, borderWidth: 2, borderColor: "#fff", backgroundColor: "red"}}></View>
            {/* <Text style={{ fontSize: 18, marginBottom: 10, color: 'white' }}> Take Picture</Text> */}
           </TouchableOpacity>
             : 
             <TouchableOpacity
             style={{
              
               alignSelf: 'flex-end',
               alignItems: 'center',
               justifyContent:"center",
               marginBottom: 50,
               width:"100%"
             }}
             onPress={this._StartRecord} >
               <View style={{height: 100, width: 100, borderRadius: 50, borderWidth: 2, borderColor: "#fff", backgroundColor: "#000"}}></View>
             {/* <Text style={{ fontSize: 18, marginBottom: 10, color: 'white' }}> Take Picture</Text> */}
            </TouchableOpacity> }
            {/* <TouchableOpacity
              style={{
                flex: 0.1,
                alignSelf: "flex-end",
                alignItems: "center"
              }}
              onPress={this._saveVideo}
            >
              <Text style={{ fontSize: 18, marginBottom: 10, color: "white" }}>
                {" "}
                Save Video
              </Text>
            </TouchableOpacity> */}
          </View>
        </Camera>
      </View>
    );
  }
}
