import React from "react";
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  DatePickerAndroid,
  TouchableOpacity,
  View,
  TextInput,
  AsyncStorage,
  TimePickerAndroid,
  ToastAndroid,
  KeyboardAvoidingView
} from "react-native";
import { Left, Right, Body, Icon, Picker, Toast } from "native-base";
import { MaterialCommunityIcons, EvilIcons, AntDesign } from "@expo/vector-icons";
import axios from "axios";

class VisitLogStart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pickerLoadedCategory: false,
      customer_name: "",
      project_name: "",
      customer_id: "",
      project_id: "",
      pickerLoadedCustomer: false,
      pickerLoadedProject: false,
      pickerLoadedSubCategory: false,
      remarksLoaded: false,
      customerData: [],
      projectData: [],
      selectedCustomer: "",
      selectedProject: "",
      visit_date: "",
      visit_time: "",
      categorydata: [],
      selectedCategory: "",
      subcategorydata: [],
      selectedsubCategory: "",
      staff_id: "",
      selectedCategoryid: "",
      remarks: "",
      remarksArr: [],
      remarks1: "",
      remarks2:"",
      visit_id: "",
      title_subtitle_remarks: "",
      log_remarks: "",
      log_recommended_action: "",
      log_user_comment: "",
      log_fees: "",
      log_fee_remarks: "",
      isCompleted:"",
      img: "",
      isSubmitted: ""
    };
  }
  componentDidMount() {
    const { navigation } = this.props;
    var project_id = navigation.getParam("project_id", "NO-ID");
    var customer_id = navigation.getParam("customer_id", "NO-ID");
    this.state.visit_date = navigation.getParam("date", "NO-ID");
    this.state.visit_time = navigation.getParam("time", "NO-ID");
    var visit_id = navigation.getParam("visit_id", "NO-ID");
    var isCompleted = navigation.getParam("isCompleted", "NO-ID");
    var isSubmitted = navigation.getParam("isSubmitted", "NO-ID");
    
    this.setState({isSubmitted})
    var img = navigation.getParam("img", "NO-ID");
    this.setState({img});
    console.log("img", img);
    this.setState({isCompleted: isCompleted});
    var title_subtitle_remarks = navigation.getParam(
      "title_subtitle_remarks",
      "NO-ID"
    );
    var log_remarks = navigation.getParam("log_remarks", "NO-ID");
    var log_recommended_action = navigation.getParam(
      "log_recommended_action",
      "NO-ID"
    );
    var log_user_comment = navigation.getParam("log_user_comment", "NO-ID");
    var log_fees = navigation.getParam("log_fees", "NO-ID");
    var log_fee_remarks = navigation.getParam("log_fee_remarks", "NO-ID");
    this.setState({ visit_id });
    this.setState({ selectedProject: project_id });
    this.setState({ selectedCustomer: customer_id });
    this.setState({ title_subtitle_remarks });
    this.setState({ log_remarks });
    this.setState({ log_recommended_action });
    this.setState({ log_user_comment });
    this.setState({ log_fees });
    this.setState({ log_fee_remarks });
    console.log("log fee",log_fee_remarks);

    if(log_fees !== "NO-ID"){
      console.log("data is present");
      var titlesubarr = [];
      titlesubarr = JSON.parse(title_subtitle_remarks);
      console.log(titlesubarr);
      this.setState({remarksArr: titlesubarr});
      var remarks12arr = JSON.parse(log_remarks);
      var remarks1 =remarks12arr[0];
      console.log(remarks12arr);
      console.log(remarks1);
      this.setState({remarksLoaded : true})
      this.setState({remarks1: remarks1});
    }
    else {
      console.log("firstvisit")
    }

    this.getCustomers();
  }
  showAndroidDatePicker = async () => {
    try {
      const { action, year, month, day } = await DatePickerAndroid.open({
        date: this.state.date
      });
      if (action !== DatePickerAndroid.dismissedAction) {
        var date = new Date(year, month, day);
        this.setState({ date: date });
        console.log(date);
        var new_month = month + 1;
        var zeroed_month = new_month < 10 ? "0" + new_month : new_month;
        var zeroed_date = day < 10 ? "0" + day : day;
        this.setState(
          { dateStr: year + "-" + zeroed_month + "-" + zeroed_date },
          this.getDateVisits
        );
      }
    } catch ({ code, message }) {
      console.warn("Cannot open date picker", message);
    }
  };

  showAndroidTimePicker = async () => {
    try {
      const { action, hour, minute } = await TimePickerAndroid.open({
        hour: 14,
        minute: 0,
        is24Hour: false // Will display '2 PM'
      });
      if (action !== TimePickerAndroid.dismissedAction) {
        this.setState({ visit_time: hour + ":" + minute });
      }
    } catch ({ code, message }) {
      console.warn("Cannot open time picker", message);
    }
  };

  async getCustomers() {
    this.state.staff_id = await AsyncStorage.getItem("staff_id");
    console.log(this.state.staff_id);
    var self = this;
    axios
      .post(global.url + "/api/getCustomers", {
        staff_id: self.state.staff_id
      })
      .then(function(response) {
        console.log("customer data");
        console.log(response.data);
        console.log(response.data.data.length);
        if (response.data.success) {
          self.setState({customerData: response.data.data});
          self.setState({ pickerLoadedCustomer: true });
        } else {
        }
        self.getProjects();
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  getProjects = () => {
    var self = this;
    axios
      .get(global.url + "/api/getProjects", {})
      .then(function(response) {
        // console.log(response.data);
        if (response.data.success) {
          self.setState({ projectData: response.data.data });
          //    console.log(self.state.customerData)
          self.setState({ pickerLoadedProject: true });
        } else {
        }
        self.getCategories();
      })
      .catch(function(error) {
        console.log(error);
      });
  };

  getCategories = () => {
    var self = this;
    axios
      .get(global.url + "/api/getCategory", {})
      .then(function(response) {
        console.log(response.data);
        if (response.data.success) {
          self.setState({ categorydata: response.data.data });
          // console.log(self.state.categorydata)
          self.setState({ pickerLoadedCategory: true });
        } else {
        }
      })
      .catch(function(error) {
        console.log(error);
      });
  };

  getSubCategories = () => {
    var self = this;
    console.log(self.state.selectedCategoryid);
    console.log(self.state.selectedCategory.split("_")[0]);
    axios
      .post(global.url + "/api/getSubCategory", {
        cat: self.state.selectedCategoryid
      })
      .then(function(response) {
        console.log(response.data);
        if (response.data.success) {
          self.setState({ subcategorydata: response.data.data });
          self.setState({ pickerLoadedSubCategory: true });
        } else {
        }
      })
      .catch(function(error) {
        console.log(error);
      });
  };

  addRemarks = () => {
    this.setState({ remarksLoaded: false });

    if(this.state.selectedCategory !=="" && this.state.selectedsubCategory !== "" && this.state.remarks !== ""){

    

    var category_id = this.state.selectedCategory.split("_")[0];
    var category_name = this.state.selectedCategory.split("_")[1];

    var subcategory_id = this.state.selectedsubCategory.split("_")[0];
    var subcategory_name = this.state.selectedsubCategory.split("_")[1];

    var remarks = this.state.remarks;

    var obj = {
      title: {
        name: category_name,
        id: category_id
      },
      subtitle: {
        name: subcategory_name,
        id: subcategory_id
      },
      remarks: remarks
    };
    this.state.remarksArr.push(obj);
    this.state.selectedCategory = "";
    this.state.selectedsubCategory = "";
    this.state.remarks = "";
    this.setState({ pickerLoadedSubCategory: false });
    console.log(this.state.remarksArr);
    this.setState({ remarksLoaded: true });
  }
  else {
    Toast.show({
      text: "Please fill all fields to add!",
      buttonText: "Okay",
      duration: 3000
    })
    // ToastAndroid.show("Please fill all fields to add!", ToastAndroid.SHORT);
    this.setState({ remarksLoaded: true });
  }
  };

  static navigationOptions = {
    header: null
  };
  render() {
    return (

      <KeyboardAvoidingView style={styles.container}  behavior="padding">
        <View
          style={{
            backgroundColor: "#1095c4",
            width: "100%",
            height: 80,
            flexDirection: "row",
paddingTop:24
          }}
        >
          <Left style={{ flex: 0.5, flexDirection:"row" }}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={{flex:1}}>
              <Icon
                name="ios-arrow-back"
                style={{ color: "#fff", marginLeft: 14 }}
              />
            </TouchableOpacity>
          </Left>
          <Body style={{ flex: 1 }}>
            <Text
              style={{
                color: "#fff",
                fontSize: 17,
                letterSpacing: 1,
                fontWeight: "bold"
              }}
            >
              VISIT LOG
            </Text>
          </Body>
          <Right style={{ flex: 0.5 }}>
            <TouchableOpacity
              onPress={() => this.props.navigation.openDrawer()}
              style={{ marginRight: 14 }}
            >
              <Icon name="md-menu" style={{ color: "#fff", marginLeft: 14 }} />
            </TouchableOpacity>
          </Right>
        </View>
        <ScrollView style={{ flex: 1 }}>
          <View style={{ margin: 12 }}>
            <View style={{ marginLeft: 12, marginRight: 12 }}>
              {this.state.pickerLoadedCustomer ? (
                <View style={{ borderColor: "#000", borderWidth: 1, padding: 12 }}>
                  {/* <Picker
                    selectedValue={this.state.selectedCustomer}
                    style={{ height: 50, width: "100%" }}
                    onValueChange={value => {
                      this.setState({ selectedCustomer: value });
                      // if (value !== "0") {

                      // }
                    }}
                  >
                    <Picker.Item
                      label="Customer Name"
                      value="0"
                      style={{ color: "red", fontSize: 30 }}
                    />
                    {this.state.customerData.map(cs => (
                      <Picker.Item
                        label={cs.customer_name}
                        value={cs.id.toString()}
                        key={cs.id.toString()}
                      />
                    ))}
                  </Picker> */}
                  <Text>{this.state.customerData.map(cs=>(
                    cs.id.toString() === this.state.selectedCustomer ? cs.customer_name : null
                  ))}</Text>
                </View>
              ) : null}
              {this.state.pickerLoadedProject ? (
                <View
                  style={{ borderColor: "#000", borderWidth: 1, marginTop: 12, padding: 12 }}
                >
                  {/* <Picker
                    selectedValue={this.state.selectedProject}
                    style={{ height: 50, width: "100%" }}
                    onValueChange={value => {
                      this.setState({ selectedProject: value });
                    }}
                  >
                    <Picker.Item
                      label="Project Name"
                      value="0"
                      style={{ color: "red", fontSize: 30 }}
                    />
                    {this.state.projectData.map(cs => (
                      <Picker.Item
                        label={cs.project_name}
                        value={cs.id.toString()}
                        key={cs.id.toString()}
                      />
                    ))}
                  </Picker> */}
                   <Text>{this.state.projectData.map(cs=>(
                    cs.id.toString() === this.state.selectedProject ? cs.project_name : null
                  ))}</Text>
                </View>
              ) : null}

              <View
                style={{
                  marginTop: 12,
                  flexDirection: "row",
                  alignItems: "center"
                }}
              >
                <TouchableOpacity
                  onPress={this.showAndroidDatePicker}
                  style={{ width: 120 }}
                >
                  <View
                    style={{
                      borderBottomWidth: 0.5,
                      borderBottomColor: "#000",

                      padding: 12
                    }}
                  >
                    <Text>{this.state.visit_date}</Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={this.showAndroidDatePicker}>
                  <EvilIcons
                    style={styles.searchIcon}
                    style={{ color: "#1095c4" }}
                    name="calendar"
                    size={28}
                    color="grey"
                  />
                </TouchableOpacity>
              </View>

              <View
                style={{
                  marginTop: 12,
                  flexDirection: "row",
                  alignItems: "center"
                }}
              >
                <TouchableOpacity
                  onPress={this.showAndroidTimePicker}
                  style={{ width: 120 }}
                >
                  <View
                    style={{
                      borderBottomWidth: 0.5,
                      borderBottomColor: "#000",

                      padding: 12
                    }}
                  >
                    <Text>{this.state.visit_time.toString()}</Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={this.showAndroidTimePicker}>
                  <EvilIcons
                    style={styles.searchIcon}
                    style={{ color: "#1095c4" }}
                    name="clock"
                    size={28}
                    color="grey"
                  />
                </TouchableOpacity>
              </View>
            </View>

            <View
              style={{
                height: 1,
                backgroundColor: "#000",
                width: "100%",
                marginTop: 16
              }}
            ></View>

            <View style={{ marginLeft: 12, marginRight: 12, marginTop: 16 }}>
              {this.state.pickerLoadedCategory ? (
                <View style={{ borderColor: "#000", borderWidth: 1 }}>
                  <Picker
                  mode="dropdown"
                  placeholder="Select task"
                  iosIcon={<Icon name="arrow-down" />}
                  headerStyle={{ backgroundColor: "#1095c4" }}
                  headerBackButtonTextStyle={{ color: "#fff" }}
                  headerTitleStyle={{ color: "#fff" }}
                  placeholder="Select a task"
                    selectedValue={this.state.selectedCategory}
                    style={{ height: 50, width: "100%" }}
                    onValueChange={(itemValue, itemIndex) => {
                      this.setState({ pickerLoadedSubCategory: false });
                      var id = itemValue.split("_")[0];
                      console.log(itemValue.split("_")[0]);
                      if (itemValue !== "0") {
                        this.setState({ selectedCategory: itemValue });
                        this.setState(
                          { selectedCategoryid: id },
                          this.getSubCategories
                        );
                      } else {
                        this.setState({ selectedCategory: itemValue });
                      }
                    }}
                  >
                    <Picker.Item label="Select a task" value="0" />
                    {this.state.categorydata.map(cs => (
                      <Picker.Item
                        label={cs.category_name}
                        value={
                          cs.category_id.toString() + "_" + cs.category_name
                        }
                        key={cs.category_id.toString()}
                      />
                    ))}
                  </Picker>
                </View>
              ) : null}
              {this.state.pickerLoadedSubCategory ? (
                <View
                  style={{ borderColor: "#000", borderWidth: 1, marginTop: 16 }}
                >
                  <Picker
                  mode="dropdown"
                  iosIcon={<Icon name="arrow-down" />}
                  headerStyle={{ backgroundColor: "#1095c4" }}
                  headerBackButtonTextStyle={{ color: "#fff" }}
                  headerTitleStyle={{ color: "#fff" }}
                  placeholder="Select a sub-task"
                    selectedValue={this.state.selectedsubCategory}
                    style={{ height: 50, width: "100%" }}
                    onValueChange={(itemValue, itemIndex) => {
                      console.log(itemValue);
                      if (itemValue !== "0") {
                        this.setState({ selectedsubCategory: itemValue });
                      } else {
                        this.setState({ selectedCategory: itemValue });
                      }
                    }}
                  >
                    <Picker.Item label="Select a sub-task" value="0" />
                    {this.state.subcategorydata.map(cs => (
                      <Picker.Item
                        label={cs.subcategory_name}
                        value={
                          cs.subcategory_id.toString() +
                          "_" +
                          cs.subcategory_name
                        }
                        key={cs.subcategory_id.toString()}
                      />
                    ))}
                  </Picker>
                </View>
              ) : null}
              <Text style={{ marginTop: 16 }}>Remarks</Text>
              <View style={{ marginTop: 8 }}>
                <TextInput
                  style={{
                    borderWidth: 1,
                    borderColor: "#000",
                    width: "100%",
                    height: 130,
                    paddingLeft: 12
                  }}
                  multiline={true}
                  onChangeText={text => this.setState({ remarks: text })}
                  value={this.state.remarks}
                ></TextInput>
              </View>
              <View  style={{ alignItems: "flex-end", marginTop: 16 }}> 

              
              <TouchableOpacity
               
                onPress={this.addRemarks}
              >
                <View
                  style={{
                    backgroundColor: "#1095c4",
                    height: 40,
                    width: 90,
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                >
                  <Text style={{ color: "#fff" }}>Add</Text>
                </View>
              </TouchableOpacity>
              </View>

              <View style={{ marginTop: 16 }}>
                <View style={{ flexDirection: "row", height: 40 }}>
                  <View
                    style={{
                      flex: 1,
                      borderWidth: 1,
                      borderColor: "#000",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Text>Task</Text>
                  </View>
                  <View
                    style={{
                      flex: 1,
                      borderWidth: 1,
                      borderColor: "#000",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Text>Sub-task</Text>
                  </View>
                  <View
                    style={{
                      flex: 1,
                      borderWidth: 1,
                      borderColor: "#000",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Text>Remarks</Text>
                  </View>
                  <View
                    style={{
                      flex: 0.5,
                      borderWidth: 1,
                      borderColor: "#000",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Text>Del</Text>
                  </View>
                </View>

                {this.state.remarksLoaded
                  ? this.state.remarksArr.map((r, i) => (
                      <View style={{ flexDirection: "row", height: 40 }}>
                        <View
                          style={{
                            flex: 1,
                            borderWidth: 1,
                            borderColor: "#000",
                            justifyContent: "center",
                            alignItems: "center"
                          }}
                        >
                          <Text>{r.title.name}</Text>
                        </View>
                        <View
                          style={{
                            flex: 1,
                            borderWidth: 1,
                            borderColor: "#000",
                            justifyContent: "center",
                            alignItems: "center"
                          }}
                        >
                          <Text>{r.subtitle.name}</Text>
                        </View>
                        <View
                          style={{
                            flex: 1,
                            borderWidth: 1,
                            borderColor: "#000",
                            justifyContent: "center",
                            alignItems: "center"
                          }}
                        >
                          <Text>{r.remarks}</Text>
                        </View>
                        <View
                          style={{
                            flex: 0.5,
                            borderWidth: 1,
                            borderColor: "#000",
                            justifyContent: "center",
                            alignItems: "center"
                          }}
                        ><TouchableOpacity onPress={()=>{
                          this.state.remarksArr.splice(i, 1);
                          this.setState({remarksArr: this.state.remarksArr})
                          console.log(i);
                        }}>
                          <AntDesign name="delete" style={{color:"red", fontSize: 18}}/>
                          </TouchableOpacity>
                        </View>
                      </View>
                    ))
                  : null}
              </View>

              <Text style={{ marginTop: 16 }}>Remarks 1</Text>
              <View style={{ marginTop: 8 }}>
                <TextInput
                  style={{
                    borderWidth: 1,
                    borderColor: "#000",
                    width: "100%",
                    height: 130,
                    paddingLeft: 12
                  }}
                  multiline={true}
                  onChangeText={text => this.setState({ remarks1: text })}
                  value={this.state.remarks1}
                ></TextInput>
              </View>

                  {this.state.log_fees === "NO-ID" ? 
              <TouchableOpacity
                style={{ alignItems: "flex-end", marginTop: 16 }}
                onPress={() =>
                  this.props.navigation.navigate("VisitLogMid", {
                    itemId: 86,
                    project_id: this.state.selectedCustomer,
                    customer_id: this.state.selectedProject,
                    log_date: this.state.visit_date,
                    log_time: this.state.visit_time,
                    remarksArr: JSON.stringify(this.state.remarksArr),
                    remarks1: this.state.remarks1,
                    visit_id: this.state.visit_id,
                    isCompleted:this.state.isCompleted
                  })
                }
              >
                <View
                  style={{
                    backgroundColor: "#1095c4",
                    height: 40,
                    width: 90,
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                >
                  <Text style={{ color: "#fff" }}>Next</Text>
                </View>
              </TouchableOpacity> : <TouchableOpacity
                style={{ alignItems: "flex-end", marginTop: 16 }}
                onPress={() =>
                  this.props.navigation.navigate("VisitLogMid", {
                    itemId: 86,
                    project_id: this.state.selectedCustomer,
                    customer_id: this.state.selectedProject,
                    log_date: this.state.visit_date,
                    log_time: this.state.visit_time,
                    remarksArr: JSON.stringify(this.state.remarksArr),
                    remarks1: this.state.remarks1,
                    visit_id: this.state.visit_id,
                    log_remarks: this.state.log_remarks,
                    log_recommended_action: this.state.log_recommended_action,
                    log_user_comment: this.state.log_user_comment,
                    log_fees: this.state.log_fees,
                    log_fee_remarks: this.state.log_fee_remarks,
                    isCompleted:this.state.isCompleted,
                    img:this.state.img,
                    isSubmitted: this.state.isSubmitted
                  })
                }
              >
                <View
                  style={{
                    backgroundColor: "#1095c4",
                    height: 40,
                    width: 90,
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                >
                  <Text style={{ color: "#fff" }}>Next</Text>
                </View>
              </TouchableOpacity>}
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //paddingTop: 24,
    backgroundColor: "#fff"
  }
});

export default VisitLogStart;
