import * as WebBrowser from "expo-web-browser";
import React from "react";
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  AsyncStorage,
  ProgressBarAndroid
} from "react-native";
import { Ionicons, AntDesign } from "@expo/vector-icons";
import { MonoText } from "../components/StyledText";
import { Left, Right, Body, Icon } from "native-base";
import { TextInput } from "react-native-gesture-handler";
import axios from "axios";

class ProjectHistory extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      project: {},
      customer: {},
      customer_id: "",
      project_id: "",
      visitHistory: {},
      staff_id: "Loremm",
      isLoading: true,
      isEmpty: false
    };
  }

  componentDidMount() {
    this.getHistory();
  }

  async getHistory() {
    this.state.staff_id = await AsyncStorage.getItem("staff_id");
    console.log(this.state.staff_id, "staff id");
    const { navigation } = this.props;
    var project_id = navigation.getParam("project_id", "NO-ID");
    var customer_id = navigation.getParam("customer_id", "NO-ID");
    console.log(project_id, customer_id, "pc");
    var self = this;
    axios
      .post(global.url + "/api/getProjectVisits", {
        staff_id: self.state.staff_id,
        customer_id: customer_id,
        project_id: project_id
      })
      .then(function (response) {
        console.log(response.data);
        self.setState({ project: response.data.project });
        if (response.data.success) {
          self.setState({ visitHistory: response.data.data });
          self.setState({ project: response.data.project });
          self.setState({ customer: response.data.customer });
          self.setState({ isLoading: false });
        } else {
          self.setState({isEmpty: true});
          self.setState({ isLoading: false });
        }
      })
      .catch(function (error) {
        console.log(error);
        self.setState({ isLoading: false });
      });
  }

  static navigationOptions = {
    header: null
  };

  render() {
    return (
      // inside return you will design UI ZOBAIR

      <View style={styles.container}>
        <View
          style={{
            backgroundColor: "#1095c4",
            width: "100%",
            height: 80,
            flexDirection: "row",
paddingTop:24
          }}
        >
          <Left style={{ flex: 0.3 }}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Icon
                name="ios-arrow-back"
                style={{ color: "#fff", marginLeft: 14 }}
              />
            </TouchableOpacity>
          </Left>
          <Body style={{ flex: 1 }}>
            <Text
              style={{
                color: "#fff",
                fontSize: 17,
                letterSpacing: 1,
                fontWeight: "bold"
              }}
            >
              PROJECT HISTORY
            </Text>
          </Body>
          <Right style={{ flex: 0.3 }}>
            <TouchableOpacity
              onPress={() => this.props.navigation.openDrawer()}
              style={{ marginRight: 14 }}
            >
              <Icon name="md-menu" style={{ color: "#fff", marginLeft: 14 }} />
            </TouchableOpacity>
          </Right>
        </View>
        {this.state.isLoading ? (
          <ProgressBarAndroid style={{ color: "#1095c4", marginTop: 12 }} />
        ) : (
          this.state.isEmpty ? <View
          style={{
            backgroundColor: "#1095c470",
            padding: 12,
            justifyContent: "center",
            alignItems: "center",
            marginTop: 12
          }}
        >
          <Text>No Visits to show!</Text>
        </View> : 
            <ScrollView style={{ flex: 1 }}>
              <View
                style={{
                  backgroundColor: "#1095c4",
                  margin: 16,
                  paddingBottom: 20,
                  borderRadius: 5
                }}
              >
                <Text
                  style={{
                    color: "white",
                    fontSize: 14,
                    fontWeight: "bold",
                    paddingLeft: 10,
                    paddingTop: 10
                  }}
                >
                  Customer Name: {this.state.customer.customer_name}
                </Text>
                <Text
                  style={{
                    color: "white",
                    fontSize: 14,
                    fontWeight: "bold",
                    paddingLeft: 10,
                    paddingTop: 5
                  }}
                >
                  Project Name: {this.state.project.project_name}
                </Text>
                <View style={{flexDirection:"row"}}>
                <Text
                  style={{
                    color: "white",
                    fontSize: 14,
                    fontWeight: "bold",
                    paddingLeft: 10,
                    paddingTop: 5
                  }}
                >
                  Project Details: 
                </Text>
                <Text  style={{
                    color: "white",
                    fontSize: 14,
                    fontWeight: "bold",
                    paddingLeft: 10,
                    paddingTop: 5
                  }}>{this.state.project.project_detail}</Text>
                </View>
                

                {this.state.visitHistory.visit.map(vs => (
                  <View key={vs.id.toString()}>
                    <View style={styles.lineStyle} />
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('VisitDetails', {
                      vs:vs,
                      customer_name: this.state.customer.customer_name,
                      project_name: this.state.project.project_name,
                    })}>
                      <View style={{ flexDirection: "row", margin:8, borderRadius: 5, padding: 6, alignItems: "center", 
                      borderWidth: 1, borderColor: "#ffffff60", width: 130, justifyContent:"center" }}>
                        <AntDesign name="infocirlce" style={{ color: "#fff", fontSize: 16 }} />
                        <Text
                          style={{
                            color: "white",
                            fontSize: 16,
                            marginLeft: 6
                          }}
                        >
                          Visit History
                </Text>
                      </View>
                    </TouchableOpacity>
                    <View
                      style={{ flexDirection: "row", alignItems: "flex-start" }}
                    >
                      <Text
                        style={{
                          color: "white",
                          fontSize: 14,
                          fontWeight: "bold",
                          paddingLeft: 10,
                          
                        }}
                      >
                        Visit Date:
                  </Text>
                      <Text
                        style={{ color: "white", fontSize: 13,  }}
                      > {vs.log_date}</Text>
                    </View>
                    <View
                      style={{ flexDirection: "row", alignItems: "flex-start" }}
                    >
                      <Text
                        style={{
                          color: "white",
                          fontSize: 14,
                          fontWeight: "bold",
                          paddingLeft: 10,
                          paddingTop: 5
                        }}
                      >
                        Visit Time:
                  </Text>
                      <Text
                        style={{ color: "white", fontSize: 13, paddingTop: 6 }}
                      > {vs.log_time}</Text>
                    </View>
                    <View
                      style={{ flexDirection: "row", alignItems: "flex-start" }}
                    >
                      <Text
                        style={{
                          color: "white",
                          fontSize: 14,
                          fontWeight: "bold",
                          paddingLeft: 10,
                          paddingTop: 5
                        }}
                      >
                        Staff Name:
                  </Text>
                      <Text
                        style={{ color: "white", fontSize: 13, paddingTop: 6 }}
                      > {vs.employee}</Text>
                    </View>
                    <Text
                      style={{
                        color: "white",
                        fontSize: 14,
                        fontWeight: "bold",
                        paddingLeft: 10,
                        paddingTop: 5
                      }}
                    >
                      Recommendation Marks:
                </Text>
                    <Text
                      style={{
                        color: "white",
                        fontSize: 13,
                        paddingTop: 3,
                        paddingLeft: 10
                      }}
                    > {vs.log_recommended_action === null ? "No remarks" : vs.log_recommended_action}</Text>
                    <Text
                      style={{
                        color: "white",
                        fontSize: 14,
                        fontWeight: "bold",
                        paddingLeft: 10,
                        paddingTop: 5
                      }}
                    >
                      Back office remarks:
                </Text>
                    <Text
                      style={{
                        color: "white",
                        fontSize: 13,
                        paddingTop: 3,
                        paddingLeft: 10
                      }}
                    >{vs.log_user_comment === null ? "No remarks" : vs.log_user_comment}</Text>
                    <View
                      style={{ flexDirection: "row", alignItems: "flex-start" }}
                    >
                      <Text
                        style={{
                          color: "white",
                          fontSize: 14,
                          fontWeight: "bold",
                          paddingLeft: 10,
                          paddingTop: 5
                        }}
                      >
                        Visit Status:
                  </Text>
                      <Text
                        style={{ color: "white", fontSize: 13, paddingTop: 6 }}
                      > {vs.log_visit_status}</Text>
                    </View>
                  </View>
                ))}
              </View>
            </ScrollView>
          )}
      </View>

    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  row: {
    flex: 1,
    flexDirection: "row"
  },
  lineStyle: {
    borderWidth: 1,
    borderColor: "white",
    marginTop: 10,
    marginHorizontal: 10
  }
});

export default ProjectHistory;
